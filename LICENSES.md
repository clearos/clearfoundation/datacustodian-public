# Open Source Licenses

`datacustodian` uses many open source projects to do what it does. This page summarizes the various
components and their licenses. Instead of including a copy of each license (which makes for a very
large document), we just include the name of the license. Interested readers can lookup the exact
license at the respective repositories to see if their are relevant modifications.

## Software Dependencies

**attrdict** (MIT): https://github.com/bcj/AttrDict
**requests** (Apache 2.0): https://2.python-requests.org/en/master/
**pyparsing** (MIT): https://github.com/pyparsing/pyparsing/
**termcolor** (MIT): http://pypi.python.org/pypi/termcolor
**tqdm** (MIT, MPL 2.0): https://github.com/tqdm/tqdm
**jinja2** (BSD-3-Clause): https://palletsprojects.com/p/jinja/
**flask** (BSD-3-Clause): https://palletsprojects.com/p/flask/
**flask-restful** (BSD-3-Clause): https://github.com/flask-restful/flask-restful/
**flask_restplus** (BSD-3-Clause): https://github.com/noirbizarre/flask-restplus/
**pyyaml** (MIT): https://github.com/yaml/pyyaml
**cloudant** (Apache 2.0): https://github.com/cloudant/python-cloudant
**gunicorn** (MIT): http://gunicorn.org/
**flask_cors** (MIT): https://github.com/corydolphin/flask-cors
**psycopg2-binary** (LGPL): http://initd.org/psycopg/
**sqlalchemy** (MIT): http://www.sqlalchemy.org/
**flask_sqlalchemy** (BSD-3-Clause): https://github.com/pallets/flask-sqlalchemy
**authlib** (BSD-3-Clause): https://authlib.org/
**qrcode** (BSD): https://github.com/lincolnloop/python-qrcode
**pillow** (HPND): https://python-pillow.org/
**aiofcm** (Apache 2.0): https://github.com/Fatal1ty/aiofcm
**aries_cloudagent** (Apache 2.0): https://github.com/hyperledger/aries-cloudagent-python
**cbor** (Apache): https://bitbucket.org/bodhisnarkva/cbor
**redis** (MIT): https://github.com/andymccurdy/redis-py
**didery** (Apache 2.0): https://github.com/reputage/didery
**pycryptodome** (BSD, Public Domain, Apache): https://www.pycryptodome.org/
**didauth** (MIT): https://github.com/PSPC-SPAC-buyandsell/didauth
**fido2** (Custom, similar to BSD): https://github.com/Yubico/python-fido2/blob/master/COPYING
**pynacl** (Apache 2.0): https://github.com/pyca/pynacl/

## Server Dependencies

These dependencies are run in the `docker-compose` environment to support `datacustodian`.

**couchdb** (Apache 2.0): https://couchdb.apache.org/
**postgres** (FOSS, GPL-compatible): https://www.postgresql.org/
**redis** (BSD): https://redis.io/