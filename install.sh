# This is an example of an install script that will get datacustodian up and running.
git@gitlab.com:clearos/clearfoundation/datacustodian-public.git datacustodian-dist
cd datacustodian-dist
pip install -r requirements.txt

git clone https://gitlab.com/clearos/clearfoundation/didauth.git clear-didauth
pip install -e clear-didauth
git clone https://gitlab.com/clearos/clearfoundation/fido.git clear-fido
pip install -e clear-fido
git clone https://github.com/rosenbrockc/aries-cloudagent-python.git
pip install -e aries-cloudagent-python

# We need to make sure the hosts file is updated for local containers for development debugging
# add the following line to /etc/hosts
sudo echo "127.0.0.1	localhost,docker,datacustodian,postgres,ledger,datacustodian-b,couchdb" >> /etc/hosts