# `datacustodian` Revision History

## Revision 1.5.0

- Prepared the repository for open sourcing under AGPLv3.
- Fully functional vault system with end-to-end encryption is working.
- Bulk import facility for existing password managers is supported via mass import API endpoint.

## Revision 1.4.0

- Many stability enhancements including watchdog for push notifications, global thread concurrency locks for database writes.
- Fully supported vault for user password management with encryption.
- Updated endpoints for interoperability with android app and chrome extension.
- Upgraded cloud agent 0.4

## Revision 1.3.0

Support for browser plug-in with push notifications is now ready.

## Revision 1.2.0

Fully implemented FIDO2 Server is now part of data custodian.

## Revision 1.1.3

Minor fixes needed to make the server work properly with HTTPS under a revers
proxy.

## Revision 1.1.0

Remodeled the threading/async functionality to support gunicorn with multiple
workers in a production setting. Fixed the errors on shutdown of the servers,
loop and dependencies.

## Revision 1.0.2

Added checking of API keys to authorization of endpoints.

## Revision 1.0.1

This is a major release that includes a deployment of the Aries Could Agent for
decentralized support with Hyperledger Indy. This release has been partially
tested, but should be treated as an alpha release. There are probably lots of bugs!

## Revision 0.3.*

Implemented the add and pin options for the latest version of `ipfscluster`.
Since these changes affect the behavior of existing calls using the same
signatures, we used a minor version increment. They are not breaking changes,
but still are a little different than before.

## Revision 0.2.1

Fixed a bug where the files returned by the GET HTTP method would be different
than those added using POST.

## Revision 0.2.0

Added support for external test data specifications so that auto-generated packages
already have unit tests in place for all generated endpoints. Updated unit tests
for full coverage.

## Revision 0.1.0

Added IPFS support and configured CI. Basic IPFS functions for adding, pinning
and unpinning are now supported. Also a local file browser with support for adding
local files to IPFS.

## Revision 0.0.0

Initialization of basic package structure including the Flask framework for
staging the REST API by YML specification files. Full unit test coverage and
CI/CD configured.
