"""Implements a SyncManager for remote procedure calls to synchronize the multiple threads that handle
API endpoint requests.
"""
import logging
import multiprocessing
from multiprocessing.managers import SyncManager
from werkzeug.exceptions import BadRequest
from werkzeug.security import gen_salt

log = logging.getLogger(__name__)

rpc_address = None
"""tuple: of `(host, port)` needed to initialize the remote manager.
"""
authkey = None
"""str: authentication key to use for remote management synchronization.
"""
manager = None
"""RPCManager: to handle synchronization of threading events across multiple load-balanced servers using RPC.
"""

qr_events = {}
"""dict: keys are pair challenge tokens;
values are :class:`threading.Event` to synchronize requests from the browser
plug-in with the mobile app requests.
"""
db_locks = {}
"""dict: keys are database document keys; values are :class:`threading.Event` for synchronizing
the database writes from multiple threads.
"""
app_db_locks = {}
"""dict: keys are database document keys; values are :class:`threading.Event` for synchronizing
the database writes from multiple threads for the app database.
"""
vault_locks = {}
"""dict: keys are database document keys; values are :class:`threading.Event` for synchronizing
the database writes from multiple threads to the user vault.
"""
push_q = None
"""queue.Queue: for synchronizing endpoints producing push notifications with
the consumer process that sends them.
"""
push_requests = {}
"""dict: keys are message identifiers,
values are :class:`threading.Event` to synchronize requests on different threads
relating to user FIDO authentication.
"""
push_results = {}
"""dict: for synchronizing push notification results; keys are correlation_id,
values are tuple of `(sucesss, failed)`.
"""
log_counts = {}
"""dict: keys are message/correlation ids. Values are `int` number of times a
log message has been emitted to reduce log noise.
"""

event_targets = {
    "qr": qr_events,
    "push": push_requests,
    "db": db_locks,
    "vault": vault_locks,
    "app": app_db_locks
}


def configure(norun=False, **rpcargs):
    """Configures the RPC server for the application.
    """
     # Now, setup the SyncManager instance for the multiprocessing Events to
    # synchronize across servers.
    global authkey, rpc_address
    host = rpcargs["host"]
    if ':' in host:
        host, port = host.split(':')
    else:
        port = rpcargs.get("port", 50000)

    authkey = rpcargs.get("authkey", gen_salt(48)).encode()    
    rpc_address = (host, port)
    start(norun=norun)


def start(norun=False):
    """Starts RPC server resources.
    """
    if not norun:
        # Now, start the RPC server on the main thread so that clean-up goes smoothly.
        m = get_manager()
        # Wait a few seconds for the manager to start before interacting with it.
        log.debug("Waiting for RPC manager to start...")
        i, q_set = 0, False
        while i < 5 and not q_set:
            try:
                pq = m.Queue()
                m.set_push_queue(pq)
                q_set = True
            except AssertionError:
                pass
            i += 1

        if not q_set:
            log.error("Could not start RPC manager after 5 seconds.")


def shutdown():
    """Shuts down the RPC server for the application.
    """
    global manager
    log.debug(f"Shutting down RPC server: {manager}.")
    if hasattr(manager, "shutdown"):
        manager.shutdown()


def _get_event(kind, key):
    """Returns a :class:`threading.Event` corresponding to the given key.

    Args:
        kind (str): one of `qr` or `push`.
        key (str): the key that the event was created under using :func:`_create_event`.
    """
    global log_counts
    if key not in log_counts:
        log_counts[key] = 0

    result = event_targets[kind].get(key)

    if log_counts[key] % 5 == 0:
        log.debug(f"RPC get_event called for {kind} and {key} => {result}. (# {log_counts[key]})")
    log_counts[key] += 1

    return result


def _create_event(kind, key, event):
    """Creates a new :class:`threading.Event` of the specified kind under `key`.

    Args:
        kind (str): one of `qr` or `push`.
        key (str): the key to create event under; see :func:`_get_event`.
        event (multiprocessing.managers.SyncManager.Event): the event to create
            in the shared space.
    """
    global event_targets
    event_targets[kind].update([(key, event)])
    log.debug(f"RPC Event created under {kind} with {key} => {event}.")


def _del_event(kind, key):
    """Deletes the :class:`threading.Event` of the specified kind under `key`.

    Args:
        kind (str): one of `qr` or `push`.
        key (str): the key to delete.
    """
    global event_targets, log_counts
    if key in log_counts:
        del log_counts[key]
    if key in event_targets[kind]:
        del event_targets[kind][key]
        log.debug(f"RPC delete event called for {kind} with {key}.")


def _set_push_queue(pq):
    """Sets the multiprocessing push queue for sending push notifications.

    Args:
        pq (queue.Queue): the queue to use for push notifications.
    """
    global push_q
    push_q = pq
    log.debug(f"Push queues initialized at RPC server with {pq}.")


def _add_push(correlation_id, did, push_tokens, title, msg, body=None, ttl=0):
    """Adds a push notification to the manager's queue.

    Args:
        correlation_id (str): unique `id` to use for correlating sending and
            results between servers.
        did (str): DID of the user that the notification is being sent to.
        push_tokens(str or list): device identifiers/token to send to.
        title (str): title to show with the notification.
        message (str): the message that will be showed in the notification.
        body (dict): optional data body to send with the notification.
        ttl (int): how long (seconds) the message will be stored on Google servers
            while attempting delivery. If this is `0`, there is no latency in
            delivering the message (it doesn't need stored first), but if it
            can't be delivered immediately, it will be deleted. Otherwise, it
            will be reattempted until this time has elapsed.
    """
    global push_q
    push_q.put((correlation_id, did, push_tokens, title, msg, body, ttl))
    log.debug(f"Added {correlation_id} push notification for {did} to queue.")


def _get_push():
    """Gets the next item off the multiprocessing queue that the consumer should
    send via push notification.
    """
    global push_q
    result = push_q.get()
    log.debug(f"Returning next item {result} from push queue.")
    return result


def _add_push_result(correlation_id, success, failed):
    """Adds the result of sending push notifications to the RPC server queue.

    Args:
        success (list): of :class:`aiofcm.common.MessageResponse`
            that are either successful.
        failed (list): of :class:`aiofcm.common.MessageResponse`
            that are either successful.
    """
    global push_results, push_q
    push_q.task_done()
    push_results.update([(correlation_id, (success, failed))])
    log.debug(f"Added push results {success} and {failed}.")


def _get_push_result(correlation_id):
    """Gets the next item off the multiprocessing queue of push notification
    send results.
    """
    global push_results, log_counts
    result = push_results.pop(correlation_id)
    if correlation_id in log_counts:
        del log_counts[correlation_id]
    log.debug(f"Returning next item {result} from push results.")
    return result


def _push_q_empty():
    """Determines whether the push queue is empty at the RPC server.
    """
    global push_q
    return push_q.empty()


def _push_result_ready(correlation_id):
    """Determines whether the push results queue is empty at the RPC server.
    """
    global push_results, log_counts
    if correlation_id not in log_counts:
        log_counts[correlation_id] = 0
    result = correlation_id in push_results

    if log_counts[correlation_id] % 5 == 0:
        log.debug(f"Is {correlation_id} ready in push_results? {result}. (# {log_counts[correlation_id]})")
    log_counts[correlation_id] += 1

    return result


def check_rpc_obj(o, correlation):
    """Checks to make sure that the specified object is not `None` and can have
    a method invoked on it using `_callmethod`.
    """
    try:
        v = o._getvalue()
        if v is not None:
            return True
    except:
        raise BadRequest(f"The RPC object with {correlation} is invalid.")


class RPCManager(object):
    """Provides access to a :class:`multiprocessing.SyncManager`, but provides explicit attributes
    for each of the registered methods so that linters know they exist.
    """
    def __init__(self):
        self.server = SyncManager(address=rpc_address, authkey=authkey)
        log.debug(f"Created synchronous RPC manager for {rpc_address} and {authkey}.")
        multiprocessing.current_process().authkey = authkey
        try:
            self.server.register("get_event")
            self.server.register("create_event")
            self.server.register("del_event")
            self.server.register("set_push_queue")
            self.server.register("add_push")
            self.server.register("get_push")
            self.server.register("push_q_empty")
            self.server.register("add_push_result")
            self.server.register("get_push_result")
            self.server.register("push_result_ready")
            self.server.connect()
        except ConnectionRefusedError:
            log.debug("Could not connect to SyncManager for multithreading, starting a new one.")
            try:
                self.server.register("get_event", callable=_get_event)
                self.server.register("create_event", callable=_create_event)
                self.server.register("del_event", callable=_del_event)
                self.server.register("set_push_queue", callable=_set_push_queue)
                self.server.register("add_push", callable=_add_push)
                self.server.register("get_push", callable=_get_push)
                self.server.register("push_q_empty", callable=_push_q_empty)
                self.server.register("add_push_result", callable=_add_push_result)
                self.server.register("get_push_result", callable=_get_push_result)
                self.server.register("push_result_ready", callable=_push_result_ready)
                log.debug("Registered get_, set_ and del_event, set_, add_push functions to multiprocessing manager.")
                self.server.start()
                log.info("SyncManager server started for FIDO multithreading.")
            except:
                log.exception(f"Trying to start new remote SyncManager at {rpc_address}")

    def __getattr__(self, attr):
        return getattr(self.server, attr)

    def shutdown(self):
        self.server.shutdown()

    def get_event(self, *args):
        return self.server.get_event(*args)

    def create_event(self, *args):
        return self.server.create_event(*args)

    def del_event(self, *args):
        return self.server.del_event(*args)

    def set_push_queue(self, *args):
        return self.server.set_push_queue(*args)

    def add_push(self, *args, **kwargs):
        return self.server.add_push(*args, **kwargs)

    def get_push(self, *args):
        return self.server.get_push(*args)

    def push_q_empty(self, *args):
        return self.server.push_q_empty(*args)

    def add_push_result(self, *args):
        return self.server.add_push_result(*args)

    def get_push_result(self, *args):
        return self.server.get_push_result(*args)

    def push_result_ready(self, *args):
        return self.server.push_result_ready(*args)


def get_manager():
    """Initializes the remote call SyncManager instance on this thread.
    """
    from datacustodian.datacustodian_app import shutdown
    global push_requests, qr_events, manager, rpc_address
    if manager is None and not shutdown.is_set():
        manager = RPCManager()
    return manager