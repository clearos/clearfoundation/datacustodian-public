# Auto-generated package module by :mod:`datakeeper`.
import logging.config
import asyncio
import signal
from os import path, environ

reporoot = path.abspath(path.expanduser(environ.get("{{ name | upper }}_HOME", '.')))
logpath = path.join(reporoot, "logging.conf")
print(f"Configuring application logging using {logpath}.")
logging.config.fileConfig(logpath, disable_existing_loggers=False)
log = logging.getLogger(__name__)
log.info("Using %s as the home directory for {{ name }}.", reporoot)

from datacustodian.utility import relpath


def run_app(norun=False, start_agents=False, gunicorn=False):
    """Runs the application in the current event loop; schedules cleanup of the
    the loop when the app exits.
    """
    from datacustodian.datacustodian_app import run
    log.info("Initializing API with specifications at %r", appspecs)
    try:
        app = run(appspecs=appspecs, norun=norun, start_agents=start_agents, gunicorn=gunicorn)
    except:
        log.exception("Initializing app from package.")
    log.debug(f"Run app call finished with result {app}.")
    return app

appspecs = [relpath(s, reporoot) for s in {{ appspecs }}]
{%- if autoload %}
app = run_app()
{%- endif %}


def configure_client_shutdown(client, loop):
    """Configures the shutdown functions for the HTTP client. Since :mod:`signal`
    needs to be configured from the main thread, this cannot be done in the
    coroutine that creates the client.
    """
    def stopper(signal, frame):
        log.info(f"Shutting down ugs client sessions gracefully using {loop}.")
        loop.run_until_complete(client.terminate())

    signal.signal(signal.SIGINT, stopper)
    signal.signal(signal.SIGTERM, stopper)


async def get_client(host="localhost", agent=None, api_key=None, timeout=20.):
    """Returns an HTTP client that can interact with these auto-generated
    endpoints.

    Args:
        host (str): HTTP hostname of the data custodian that this client will
            be interacting with.
        agent (str): name of the configured agent to use for cryptographic
            operations. This is required if the endpoints need did-based
            authentication.
        api_key (str): API key to use in the request header.
        timeout (float): number of seconds to wait for cloud agent to initialize
            before failing HTTP client creation.
    """
    from datacustodian.datacustodian_app import register_client
    from .client import HTTPClient
    loop = asyncio.get_event_loop()
    result = HTTPClient(host, agent, api_key)

    if await result.detect_agent():
        log.info("Found a running data custodian with cloud agent.")
        # We still need to initialize the specifications and objects though.
        run_app(norun=True, start_agents=False)
        # Add a terminate signal for the HTTP client.
        register_client("ugs", result, loop)
        return result
    else:
        # We only need to start the cloud agents for the client to work properly.
        log.info("Starting a new data custodian and cloud agent instance.")
        run_app(norun=True, start_agents=True)
        # Add a terminate signal for the HTTP client.
        register_client("ugs", result, loop)

    # Before we return the client, we need to make sure the agent is up and
    # running; otherwise none of the crypto functionality will work.
    from datacustodian import application_ready
    wait_count = 0
    ready = application_ready()
    while not ready and wait_count < int(timeout/2.) + 1:
        await asyncio.sleep(2.)
        wait_count += 1

    if ready and await result.detect_agent():
        return result
    else:
        log.error("Could not initilize client because cloud agent isn't ready.")
