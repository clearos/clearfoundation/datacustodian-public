"""Auto-generated testing file by datacustodian; make sure to define request data
for each of the test functions created below.
"""
import pytest
import logging
import asyncio

from datacustodian import loops
from datacustodian.testing import urlsub, conform_args, handle_client_response, filter_model_values
from urllib.parse import urlencode

shared_state = {}
"""dict: keys are `.`-separated path to unit test model results from another
unit test to share state between tests. This breaks the "indpendence" of the tests
in a theoretical sense; however, certain unit tests are dependent by design
(for example, `oauth`).
"""

log = logging.getLogger(__name__)

{%- macro async_test_stub(nspec, espec, sattr) -%}
{%- set routefun = espec.routes[0]|route2fun %}
async def stub_{{nspec.name}}_{{sattr}}{{routefun}}(http_client, *args, **kwargs):
    result = None
    async with http_client.{{nspec.name}}.{{sattr}}{{routefun}}(*args, **kwargs) as r:
        result, code = await handle_client_response(r)
        if result is not None:
            result = await result

    return result, code
{%- endmacro %}
{%- macro render_shared_state(state, nspec) -%}
    {%- for varset in state %}
    {%- set varname = varset["name"] %}
    {%- set espec = endpoints_by_name[nspec.name][varset["endpoint"]] %}
    {%- set routefun = espec.routes[0]|route2fun %}
    {%- set sattr = varset["method"] %}
    {{ varname }} = shared_state["{{sattr}}{{routefun}}.{{varname}}"]
    {%- endfor %}
{%- endmacro %}
{%- for nspec in namespaces %}
{%- for espec in nspec.endpoints %}
{%- set endpoint_test_spec = testing.get(nspec.name, {}).get("endpoints", {}).get(espec.name, {}) %}
{%- for sattr in endpoint_test_spec.get("order", ["put", "get", "post", "delete", "options", "head"]) if sattr in espec %}
{%- if testing.get(nspec.name, {"endpoints": {espec.name: {}}})["endpoints"][espec.name].get(sattr, {}).get("wire", True) %}
{{ async_test_stub(nspec, espec, sattr) }}
{%- endif %}

{%- set routefun = espec.routes[0]|route2fun %}
def test_{{nspec.name}}_{{sattr}}{{routefun}}(http_client):
    """Handles the async testing of `client.{{nspec.name}}.{{sattr}}{{routefun}}`
    using the appropriate event loop in the appropriate thread.
    """
    global shared_state
    {%- if nspec.name not in testing or espec.name not in testing[nspec.name]["endpoints"] %}
    raise NotImplementedError("Define the request arguments for this test!")
    {%- else %}
        {%- set endpoint_test_spec = testing[nspec.name]["endpoints"][espec.name] %}
        {%- if sattr in endpoint_test_spec %}
            {%- set test_spec = endpoint_test_spec[sattr] %}
    timeout = {{ test_spec.get("timeout", 5) }}
    expected_code = {{ test_spec.get("code") }}
    model = {{ test_spec.get("model") }}
    args = {{ test_spec.get("args", []) }}
    kwargs = {{ test_spec.get("kwargs", {}) }}
            {%- if test_spec["preamble"] %}
    {{ render_shared_state(test_spec.get("state", []), nspec) }}
{{ test_spec["preamble"] }}
            {%- endif %}
        {%- else %}
    timeout, expected_code = 5, None
    args, kwargs = [], {}
        {%- endif %}
    rargs, rkwargs = conform_args(args, kwargs)

    {%- if sattr in endpoint_test_spec and endpoint_test_spec[sattr].get("wire", True) %}
    future = asyncio.run_coroutine_threadsafe(stub_{{sattr}}{{routefun}}(http_client, *rargs, **rkwargs), loops["{{name}}"])
    result, code = future.result(timeout)
    {%- endif %}
    if expected_code is not None:
        assert code == expected_code
    if model is not None:
        store = filter_model_values(model, result)
        # Also handle the case where the output is a scalar string to store.
        if not isinstance(result, dict) and len(store) == 1:
            result = {store[0]: result}
        updater = {f"{{sattr}}{{routefun}}.{k}": result[k] for k in store}
        shared_state.update(updater)
        log.info(f"Shared state for {{sattr}}{{routefun}} set to {updater}.")
    {%- endif %}
{% endfor %}

{%- endfor %}
{% endfor %}
