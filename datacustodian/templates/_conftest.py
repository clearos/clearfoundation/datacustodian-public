import pytest
import asyncio

@pytest.fixture
def client():
    from {{ name }} import app
    from datacustodian.base import set_testmode
    set_testmode(True)

    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client


@pytest.fixture(scope='session', autouse=True)
def event_loop():
    """A session-scoped event loop."""
    policy = asyncio.get_event_loop_policy()
    res = policy.new_event_loop()
    asyncio.set_event_loop(res)
    res._close = res.close
    res.close = lambda: None

    yield res

    res._close()


@pytest.fixture(scope="session")
def http_client(event_loop):
    """Returns a package-specific HTTPClient object that can handle the threading
    complexity of the packages.
    """
    from ugs import get_client
    return event_loop.run_until_complete(get_client("http://datacustodian:8889", agent="default"))
