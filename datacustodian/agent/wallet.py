import logging
from datacustodian.base import AgentSectionBase
from datacustodian.crypto import bytes_to_b64, b64_to_bytes

log = logging.getLogger(__name__)

class Section(AgentSectionBase):
    async def ls(self, **kwargs):
        """Queries the `/wallet/did` endpoint to list DIDs in the wallet of the
        agent.

        Examples:
            List all the DIDs in the wallet.

            >>> await agent.wallet.ls()
            {
              "results": [
                {
                  "did": "2C3zPtmvHgk91KLxqg63iz",
                  "verkey": "edrchXRJiMioFY47ic1ezS8WzvhwKk48392gR4KAmgv",
                  "public": "true"
                }
              ]
            }
        """
        r = await self.request("GET", "/wallet/did", **kwargs)
        return r["results"]

    async def create(self, **kwargs):
        """Queries the `/wallet/did/create` endpoint to create a new DID.

        Examples:
            Create a new DID in the local wallet. This does *not* post the did
            to the ledger.

            >>> await agent.wallet.create()
            {
              "result": {
                "did": "9TBnu8U8EUfsnnsjwFLhik",
                "verkey": "5cAkN3qKsREofYcPZPKrcNJ7g1YM1a5LyJN9B9kcGz5J",
                "public": "false"
              }
            }
        """
        r = await self.request("POST", "/wallet/did/create", **kwargs)
        return r["result"]

    async def set_public(self, did, **kwargs):
        """Queries the `/wallet/did/public` endpoint to set the public DID of
        the wallet. That is the DID the wallet will use to interact with the
        ledger by signing requests.

        Args:
            did (str): the DID that should be set as public on the wallet.

        Examples:
            Set the public DID on the wallet.

            >>> await agent.wallet.set_public("9TBnu8U8EUfsnnsjwFLhik")
            {
              "result": {
                "did": "9TBnu8U8EUfsnnsjwFLhik",
                "verkey": "5cAkN3qKsREofYcPZPKrcNJ7g1YM1a5LyJN9B9kcGz5J",
                "public": "true"
              }
            }
        """
        params = {"did": did}
        r = await self.request("POST", "/wallet/did/public", params=params, **kwargs)
        return r["result"]

    async def get_public(self, **kwargs):
        """Queries the `/wallet/did/public` endpoint to get the public DID of
        the wallet. That is the DID the wallet will use to interact with the
        ledger by signing requests.

        Examples:
            Get the public DID on the wallet.

            >>> await agent.wallet.get_public()
            {
              "result": {
                "did": "2C3zPtmvHgk91KLxqg63iz",
                "verkey": "edrchXRJiMioFY47ic1ezS8WzvhwKk48392gR4KAmgv",
                "public": "true"
              }
            }
        """
        r = await self.request("GET", "/wallet/did/public", **kwargs)
        return r["result"]

    async def sign(self, message=None, verkey=None, **kwargs):
        """Queries the `/wallet/sign-message` endpoint to sign the message using
        the private key (stored in the wallet) corresponding to `verkey`.

        Args:
            message (bytes or str):  the message to sign.
            verkey (str): public key whose private key should be used to sign the
                message.

        Examples:
            Sign a message.

            >>> await agent.wallet.sign("test message", "FQTn2kCNuvxsKDHvHMyci41nXH4Gds6NGFffQhyvDcsq")
            {
              "signature": "mRWOTxZ26J2We627aGvkqAJAETZhNLXtzYzavrGW13G7WNcFhoCuRqr7IFWEGLJOd1Q/PvNqoFnIdO9F5XKmBw==",
              "message": "dGVzdCBtZXNzYWdl",
              "from_verkey": "FQTn2kCNuvxsKDHvHMyci41nXH4Gds6NGFffQhyvDcsq"
            }
        """
        assert message is not None and verkey is not None
        if not isinstance(message, bytes):
            message = message.encode("utf-8")

        body = {
          "message": bytes_to_b64(message),
          "from_verkey": verkey
        }
        r = await self.request("POST", "/wallet/sign-message", data=body, **kwargs)
        return r

    async def verify(self, message=None, verkey=None, signature=None, **kwargs):
        """Queries the `/wallet/sign-message` endpoint to sign the message using
        the private key (stored in the wallet) corresponding to `verkey`.

        Args:
            message (str): base-64 encoded message that was signed.
            verkey (str): public key whose private key was used to sign the
                message.
            signature (str): resulting signature to verify.

        Examples:
            Verify a message signature..

            >>> msg
            {
              "signature": "mRWOTxZ26J2We627aGvkqAJAETZhNLXtzYzavrGW13G7WNcFhoCuRqr7IFWEGLJOd1Q/PvNqoFnIdO9F5XKmBw==",
              "message": "dGVzdCBtZXNzYWdl",
              "from_verkey": "FQTn2kCNuvxsKDHvHMyci41nXH4Gds6NGFffQhyvDcsq"
            }
            >>> await agent.wallet.verify(**msg)
            {
              "verified": true
            }
        """
        if isinstance(message, bytes):
            message = bytes_to_b64(message)
        if isinstance(signature, bytes):
            signature = bytes_to_b64(signature)

        body = {
          "message": message,
          "from_verkey": verkey,
          "signature": signature
        }
        r = await self.request("POST", "/wallet/verify-message", data=body, **kwargs)
        return r["verified"]
