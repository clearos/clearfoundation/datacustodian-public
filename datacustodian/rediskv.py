"""Implements a wrapper around the redis server for memory caching. Objects are serialized using
:mod:`cbor` to improve speed.
"""
import cbor
import redis
from attrdict import AttrDict
import logging


log = logging.getLogger(__name__)


class RedisKV(object):
    """Represents a remote key-value store database with simple access via python get/set item methods.
    """
    def __init__(self, db_id):
        self.client = redis.Redis(host='redis', port=6379, db=db_id)

    def expire(self, *args):
        return self.client.expire(*args)

    def exists(self, *args):
        return self.client.exists(*args)

    def __getitem__(self, key):
        return self.load(key)

    def __setitem__(self, key, value):
        self.save(key, value)

    def save(self, dbid, obj):
        """Saves the specified object to the Redis key-value store.

        Args:
            db (str): short name of the db to save in.
            dbid (str): globall unique (in that database) key to store under.
            obj: object to store on the server. Will be serialized using :mod:`cbor`.
        """    
        log.debug(f"Saving redis cache entry for {dbid}.")
        self.client.set(dbid, cbor.dumps(obj))


    def load(self, dbid):
        """Returns a deserialized object from a remote key-value store. Inverse of :func:`save`.

        Args:
            db (str): short name of the db to restore from.
            dbid (str): globally unique (in that database) key to retrieve.
        """
        v = self.client.get(dbid)
        if v is not None:
            log.debug(f"Deserializing redis cache entry for {dbid}.")
            return cbor.loads(v)


dbnames = ["app", "acl", "fido", "vault"]
"""list: of `str` database names supported by the application.
"""
stores = AttrDict()
"""attrdict.AttrDict: keys are database names in :data`dbnames`; values are instances of :class:`RedisKV` for
accessing the remote key-value store.
"""

if len(stores) == 0:
    try:
        for i, dbname in enumerate(dbnames):
            stores[dbname] = RedisKV(i)
    except:
        log.exception("While initializing remote Redis key-value stores.")