__version__ = [1, 2, 1]
"""list: of `[major, minor, bugfix]` version numbers for the package.
"""
# from gevent import monkey
# monkey.patch_all()

import asyncio
import threading
import logging
import time

log = logging.getLogger(__name__)

loops = {}
"""dict: keys are thread names; values are :class:`asyncio.AbstractEventLoop`
event loops running in a separate thread for handling the async aspects of the
web server.
"""
loop_threads = {}
"""dict: keys are therad names; values are :class:`threading.Thread` that
contains the running event loop.
"""
loop_events = {}
"""dict: keys are thread names; values are :class:`threading.Event` to track
whether the loop (in the thread with the same name) has been initialized and is
running.
"""

def _create_loop(name, debug):
    """Creates a new event loop on whatever thread this function is run on.

    .. note:: The loop is set to `run_forever` after it is created. Submit
        coroutines using :func:`run_coroutine_threadsafe`.

    Args:
        name (str): name given to thread that the loop will run on.
    """
    global loops
    try:
        policy = asyncio.get_event_loop_policy()
        loops[name] = policy.new_event_loop()
        asyncio.set_event_loop(loops[name])
        loop_events[name].set()
        log.debug(f"Loop {name} has debug={debug}.")
        loops[name].set_debug(True)
        log.debug(f"Calling `run_forever` in loop {name}.")
        loops[name].run_forever()
        log.debug(f"Loop {name} is no longer running.")
    except:
        log.exception(f"In thread that started event loop {name}.")


def create_loops(master=True, components=None, debug=False):
    """Creates new event loops in a separate threads for handling async functionality.

    Args:
        master (bool): when True, also create a master loop.
        components (list): of `str` component names to create loops for; the names
            are given to the threads as well.
    """
    global loop_thread, master_loop_name, loop_events
    targets = []
    if master:
        targets.append("master")
    if components is not None:
        targets.extend(components)

    for name in targets:
        loop_events[name] = threading.Event()
        loop_threads[name] = threading.Thread(target=_create_loop, name=name, args=(name, debug))
        log.debug(f"Starting new thread for loop {name}.")
        # Start the thread and wait for it to finish initializing.
        loop_threads[name].start()
        loop_events[name].wait()

    if master:
        loops["default"] = asyncio.get_event_loop()
        loops["default"].set_debug(debug)

def stop_all_loops(future):
    """Stops all the event loops in their threads.
    """
    for name, _loop in loops.items():
        log.debug(f"Stopping event loop {name}.")
        try:
            _loop.call_soon_threadsafe(_loop.stop)
        except:
            log.exception(f"Could not stop event loop {name}. Continuing...")

    # Next, wait for all the loop threads to join, meaning that the loops have
    # actually stopped.
    for name, _thread in loop_threads.items():
        if _thread.is_alive():
            try:
                log.debug(f"Joining thread {name} on shutdown.")
                _thread.join()
            except:
                log.debug(f"Could not join thread {name} on shutdown; continuing.")
        if not _thread.is_alive():
            log.debug(f"Loop stop succeeded in thread {name}.")

def get_master_loop():
    """Returns the first event loop created in a separate thread (with index 0).
    """
    loop_events["master"].wait(5)
    return loops["master"]

def get_event_loop(name):
    """Returns the event loop with the given index.

    Args:
        name (str): name of the component to get an event loop for.
    """
    return loops.get(name)

def get_version():
    """Returns the version of the package formatted as a string.
    """
    return '.'.join(map(str, __version__))

def application_ready():
    """Checks to see if the application has finished initializing *and* the
    agents have all finished initializing by checking the :class:`threading.Event`
    flags.
    """
    from datacustodian.datacustodian_app import started
    from datacustodian.dlt import agent_events
    agents_ready = False
    for agent_name, event in agent_events.items():
        agents_ready = agents_ready and event.is_set()
        if not agents_ready:
            break

    return agents_ready and started.is_set()

def abort():
    """Sends `SIGINT` to the application.
    """
    # Only trigger the interrupt signal if shutdown isn't happening yet.
    from datacustodian.datacustodian_app import shutdown
    if not shutdown.is_set():
        import os, signal
        os.kill(os.getpid(), signal.SIGINT)

from datacustodian.datacustodian_app import configure_shutdown
