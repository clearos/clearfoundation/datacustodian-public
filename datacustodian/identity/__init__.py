"""This package deals with decentralized identity, granting of identity
credentials, quantifying similarity of identity graphs, and providing
passwordless login using FIDO and DID-based authentication.
"""
# There is a bug in multiprocessing in python3.7 and below. This monkey patches
# the `AutoProxy` needed by the SyncManager RPC server.
import multiprocessing.managers

# Backup original AutoProxy function
backup_autoproxy = multiprocessing.managers.AutoProxy

# Defining a new AutoProxy that handles unwanted key argument 'manager_owned'
def redefined_autoproxy(token, serializer, manager=None, authkey=None,
          exposed=None, incref=True, manager_owned=True):
    # Calling original AutoProxy without the unwanted key argument
    return backup_autoproxy(token, serializer, manager, authkey,
                            exposed, incref)

# Updating AutoProxy definition in multiprocessing.managers package
multiprocessing.managers.AutoProxy = redefined_autoproxy
