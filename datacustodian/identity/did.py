"""Functions to support DID registration, signatures, provenance, etc. for
identity-based operations.
"""
import logging
import json
from werkzeug.exceptions import Unauthorized

from datacustodian.dlt import Did
from datacustodian.consent.auth import auth_endpoint_roles
from datacustodian.crypto import verify_signature, b58_to_bytes
from datacustodian.db import get_view, dbget, dbupdate, dblock, dbdel, dbexists

databases = None
"""attrdict.AttrDict: databases for application, acl and fido; this variable is
set during :func:`configure`.
"""
agents = None
"""dict: of configured cloud agent clients. Set during :func:`configure`.
"""

log = logging.getLogger(__name__)


def configure(dcdbs, **idargs):
    """Configures the database and other resources for DID management.
    """
    global databases, agents
    databases = dcdbs

    from datacustodian.dlt import agents as dlt_agents
    agents = dlt_agents


async def agent_ready(_agent=None, **kwargs):
    """Tests if the specified agent is ready to accept requests.
    """
    from datacustodian.dlt import agent_events
    if agent_events[_agent].is_set():
        ready = True
    else:
        agent = agents[_agent]
        try:
            await agent.detect_process()
            ready = True
        except:
            ready = False

    return {
        "agent": _agent,
        "ready": ready
    }

def find_keys(did):
    """Searches the `app` database for a `verkey` that matches the given DID. It may be 
    home master server key, or an app derived key.

    Args:
        did (str): the DID corresponding to the verkey to look up.
    """
    dbid = f"keys/{did}"
    result = None
    doc = dbget("app", dbid)
    if doc is not None:
        if "signer" in doc:
            result = doc["signer"]

    if result is None:
        # Next, check the app-specific DIDs for a verkey.
        view = get_view("did", "packages_view")
        r = view(key=did)["rows"]
        if len(r) > 0:
            # We just take the first one we find; app DIDs preserve privacy, so there shouldn't be
            # more than one...
            result = r[0]["value"]["signer"]

    return result


def _signature_verify(signature, verkey):
    """Verifies a signature against a public key. If it fails, a :class:`Unauthorized` exception
    is raised on the endpoint.
    """
    if not verify_signature(signature, verkey):
        raise Unauthorized(f"The signature does not match {verkey}.")

def app_keys(_data=None, _request=None, **kwargs):
    """Registers the latest application cryptographic public key from a mobile app in the OS.
    """
    did, package = _data["did"], _data["package"]
    signer, ensuer, erster = _data["signer"], _data["ensuer"], _data.get("erster")
    signer_did = _data["signerDid"]
    
    # First of all, verify the signatures in the headers
    log.debug("Verifying signer headers for app key publishing.")
    _signature_verify(_request.headers["signer-sign"], signer)
    if erster is not None:
        log.debug("Verifying erster signature in headers.")
        _signature_verify(_request.headers["erster-sign"], erster)

    dbid = f"packages/{did}/{package}"
    operation = None
    if dbexists("app", dbid):
        with dblock("app", dbid) as doc:
            # We need to verify that this signer is the one that was previously registered as
            # the next rotation key, or that it is identical to the current one.
            log.debug("Rotating application keys with pre-rotation checks.")
            assert doc["signer"] == erster or doc["signer"] == signer
            if "signer_did" not in doc:
                doc["signer_did"] = signer_did
            if doc["signer"] != signer:
                # We are performing a rotation operation; update the fields and check that everything
                # matches our expectations (the new signer is our previously declared ensuer).
                assert doc["ensuer"] == signer

                # Store the current signer in the history so we can replay the rotation history
                # if necessary.
                if "history" not in doc:
                    doc["history"] = []
                doc["history"].append(doc["signer"])

                doc["signer"] = signer
                doc["signer_did"] = signer_did
                doc["ensuer"] = ensuer
                operation = "rotation"
    else:
        log.debug("Creating application key entries with pre-rotated key.")
        def doc_update():
            with dblock("app", dbid) as doc:
                doc["signer"] = signer
                doc["signer_did"] = signer_did
                doc["ensuer"] = ensuer
            return "store"

        try:
            operation = doc_update()
        except json.JSONDecodeError:
            dbdel("app", dbid)
            operation = doc_update()

    return {
        "success": True,
        "operation": operation
    }

def _register_did_user(did, name, display_name, icon):
    """Registers the user data that should be associated with the given `did` for this
    home server.

    Args:
        did (str): the Base58-encoded DID for the user.
        name (str): name to associate with the DID at home server.
        display_name (str): display name to use for the DID at home server.
        icon (str): URL to an icon to display for the user's DID.
    """
    dbid = f"users/{did}"
    with dblock("app", dbid) as doc:
        doc["name"] = name
        doc["displayName"] = display_name
        doc["icon"] = icon


def get_did_user(did):
    """Returns the user information for a given DID as a FIDO-compatible dictionary.

    Args:
        did (str): Base58-encoded DID identifier.
    """
    dbid = f"users/{did}"
    result = {
        'id': b58_to_bytes(did),
        'name': "Unregistered DID",
        'displayName': "Unregistered DID",
        'icon': None
    }

    doc = dbget("app", dbid)
    if doc is not None:
        result.update({k: v for k, v in doc.items() if k in ["name", "displayName", "icon"]})

    return result


def set_master_key(_data=None, _request=None, **kwargs):
    """Publishes the first master key and DID for a user on a new phone. This is distinct
    from a rotation operation that happens separately.
    """
    did, verkey, serial = _data["did"], _data["verkey"], _data["serial"]
    ensuer = _data["ensuer"]

    # Verify the signature from the own-your-phone for this serial number.
    # 1) Lookup the serial verkey for this serial number in the phone database.
    # 2) Make sure that the signature matches the verkey.
    log.debug("Verifying signature for own-your-phone check.")
    # TODO: check that the verkey matches the serial number in some database that
    # we don't have access to right now.
    _signature_verify(_request.headers["serial-sign"], _data["serial-key"])

    dbid = f"keys/{did}"
    log.debug(f"Storing pre-rotation and public key information for {did} registration.")
    def doc_update():
        with dblock("app", dbid) as doc:
            # Append the serial number of the phone that is being registered to this DID.
            if "serial" not in doc:
                doc["serial"] = []
            if serial not in doc["serial"]:
                doc["serial"].append(serial)

            # Next, register the signing key for this DID on this home server.
            doc["signer"] = verkey
            doc["ensuer"] = ensuer

    try:
        doc_update()
    except json.JSONDecodeError:
        dbdel("app", dbid)
        doc_update()

    # Now that the DID is registered, we also want to update the FIDO application
    # database to have the correct name, displayName and icon.
    name, display_name, icon = _data["name"], _data["displayName"], _data["icon"]
    log.debug(f"Registering user information with {name} for {did}.")
    _register_did_user(did, name, display_name, icon)

    return {
        "success": True
    }

def rotate_master_key(_data=None, _request=None, **kwargs):
    """Rotates the master key currently stored at this home server.
    """
    did, erster, signer, ensuer = _data["did"], _data["erster"], _data["signer"], _data["ensuer"]

    # Verify all the signatures for the current signer and the previous one.
    log.debug("Verifying new signer signature in headers.")
    _signature_verify(_request.headers["signer-sign"], signer)
    log.debug("Verifying erster signature in headers.")
    _signature_verify(_request.headers["erster-sign"], erster)

    dbid = f"keys/{did}"
    with dblock("app", dbid) as doc:
        assert doc["ensuer"] == signer
        assert doc["signer"] == erster

        # Keep track of the history of the key rotation events for this server.
        if "history" not in doc:
            doc["history"] = []
        doc["history"].append(doc["signer"])

        doc["signer"] = signer
        doc["ensuer"] = ensuer
