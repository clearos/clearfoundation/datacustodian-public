import logging
import sqlalchemy
from os import path
import sys

from authlib.flask.oauth2 import AuthorizationServer, ResourceProtector
from authlib.flask.oauth2.sqla import (
    create_query_client_func,
    create_save_token_func,
    create_revocation_endpoint,
    create_bearer_token_validator,
)
from authlib.oauth2.rfc6749 import grants
from werkzeug.security import gen_salt
from authlib.oidc.core.grants import (
    OpenIDCode as _OpenIDCode,
    OpenIDImplicitGrant as _OpenIDImplicitGrant,
    OpenIDHybridGrant as _OpenIDHybridGrant,
)
from authlib.oidc.core import UserInfo

from .models import db, User, configure_user_model
from .models import OAuth2Client, OAuth2AuthorizationCode, OAuth2Token, OIDCAuthorizationCode

log = logging.getLogger(__name__)


query_client = None
"""typing.Callable: function that returns a :class:`models.OAuth2Client` using
a client_id.
"""
save_token = None
"""typing.Callable: function that saves a :class:`models.OAuth2Token` to the
database.
"""
authorization = None
"""authlib.flask.oauth2.AuthorizationServer: to implement the OAuth protocol
according to the RFCs.
"""
require_oauth = None
"""authlib.flask.oauth2.ResourceProtector: decorator that can be placed on
endpoints to require a valid OAuth token (with valid scope authorization) before
the endpoint will work correctly.
"""
jwt_config = None
"""dict: with the key-value pairs to configure JWT. Includes keys for
`key`, `alg`, `iss`, `exp` which are the secret key, crypto algorithm,
issuer URI, and expiration time (in seconds).
"""
scopes = {}
"""dict: keys are scope names from the application specification; values are
lists of attributes names included in :data:`user_info_map` and :data:`user_info_fields`
"""

def exists_nonce(nonce, req):
    """Determines if an OpenID authorization code with the given nonce exists
    in the database.
    """
    exists = OIDCAuthorizationCode.query.filter_by(
        client_id=req.client_id, nonce=nonce
    ).first()
    return bool(exists)

user_info_map = {
   'sub': 'did'
}
"""dict: keys are OpenID registered attributes; values are the attribute names
on the :class:`model.User` object.
"""
user_info_fields = ['name', 'given_name', 'family_name', 'middle_name', 'nickname',
                     'preferred_username', 'profile', 'picture', 'website', 'email',
                     'email_verified', 'gender', 'birthdate', 'zoneinfo', 'locale',
                     'phone_number', 'phone_number_verified', 'address', 'updated_at']
"""list: of `str` OpenID registered attributes that can be included for a user,
depending on the authorized scope.
"""

def filter_user_fields(user, scope):
    """Filters the available user attributes based on the authorized scope.
    """
    attrs = {}
    for field in user_info_fields:
        if field in scopes[scope] and hasattr(user, field):
            attrs[field] = getattr(user, field)

    for field, target in user_info_map.items():
        if field in scopes[scope] and hasattr(user, target):
            attrs[field] = getattr(user, target)

    return attrs

def generate_user_info(user, scope):
    """Creates an OpenID user object from a regular :class:`models.User`.
    """
    attrs = filter_user_fields(user, scope)
    return UserInfo(**attrs)

def create_authorization_code(client, grant_user, request, kind):
    """Creates an authorization code for either standard OAuth or OpenID and
    saves it to the database.

    Args:
        client (OAuth2Client): client requesting the authorization.
        grant_user (User): user granting authorization to the client.
        request (flask.request): request object from the HTTP request.
        kind (str): one of `oauth` or `oidc` indicating which flow to use.

    Returns:
        str: salt generated as the actual code.
    """
    code = gen_salt(48)
    kwargs = dict(
        code=code,
        client_id=client.client_id,
        redirect_uri=request.redirect_uri,
        scope=request.scope,
        user_id=grant_user.did,
    )
    if kind == "oauth":
        item = OAuth2AuthorizationCode(**kwargs)
    elif kind == "oidc":
        nonce = request.data.get('nonce')
        item = OIDCAuthorizationCode(nonce=nonce, **kwargs)
    db.session.add(item)
    db.session.commit()
    return code

class _AuthorizationCodeGrant(grants.AuthorizationCodeGrant):
    kind = None
    """str: one of `oauth` or `oidc` representing standard OAuth flow or
    OpenID connect flow for authorization codes.
    """
    def create_authorization_code(self, client, grant_user, request):
        return create_authorization_code(client, grant_user, request, self.kind)

    def parse_authorization_code(self, code, client):
        cls = OAuth2AuthorizationCode if self.kind == "oauth" else OIDCAuthorizationCode
        item = cls.query.filter_by(code=code, client_id=client.client_id).first()
        if item and not item.is_expired():
            return item

    def delete_authorization_code(self, authorization_code):
        db.session.delete(authorization_code)
        db.session.commit()

    def authenticate_user(self, authorization_code):
        return User.query.get(authorization_code.user_id)

class OAuthAuthorizationCodeGrant(_AuthorizationCodeGrant):
    kind = "oauth"
class OIDCAuthorizationCodeGrant(_AuthorizationCodeGrant):
    kind = "oidc"

class PasswordGrant(grants.ResourceOwnerPasswordCredentialsGrant):
    def authenticate_user(self, username, password):
        user = User.query.filter_by(username=username).first()
        if user is not None and user.check_password(password):
            return user

class OpenIDCode(_OpenIDCode):
    def exists_nonce(self, nonce, request):
        return exists_nonce(nonce, request)

    def get_jwt_config(self, grant):
        return jwt_config

    def generate_user_info(self, user, scope):
        return generate_user_info(user, scope)

class OpenIDImplicitGrant(_OpenIDImplicitGrant):
    def exists_nonce(self, nonce, request):
        return exists_nonce(nonce, request)

    def get_jwt_config(self, grant):
        return jwt_config

    def generate_user_info(self, user, scope):
        return generate_user_info(user, scope)

class OpenIDHybridGrant(_OpenIDHybridGrant):
    def create_authorization_code(self, client, grant_user, request):
        return create_authorization_code(client, grant_user, request, "oidc")

    def exists_nonce(self, nonce, request):
        return exists_nonce(nonce, request)

    def get_jwt_config(self):
        return jwt_config

    def generate_user_info(self, user, scope):
        return generate_user_info(user, scope)

class RefreshTokenGrant(grants.RefreshTokenGrant):
    def authenticate_refresh_token(self, refresh_token):
        token = OAuth2Token.query.filter_by(refresh_token=refresh_token).first()
        if token and token.is_refresh_token_active():
            return token

    def authenticate_user(self, credential):
        return User.query.get(credential.user_id)

    def revoke_old_credential(self, credential):
        credential.revoked = True
        db.session.add(credential)
        db.session.commit()

def _create_db(dburi):
    """Creates the database for the oauth application, if it doesn't already
    exist.
    """
    log.debug("Connecting to postgres engine through SQLAlchemy.")
    pguri = path.join(path.dirname(dburi))
    dbname = path.split(dburi)[1]
    engine = sqlalchemy.create_engine(pguri)

    # Query for existing databases.
    conn = engine.connect()
    try:
        r = conn.execute("SELECT datname FROM pg_database")
        xdbs = [d[0] for d in r.fetchall()]
        r.close()
    finally:
        conn.close()
    log.debug(f"Found {xdbs} in postgres.")

    # Create database if not exists
    if dbname not in xdbs:
        log.debug(f"Executing create database for `{dbname}`.")
        conn = engine.connect()
        try:
            conn.execute("commit")
            conn.execute(f"CREATE DATABASE {dbname};")
        finally:
            conn.close()

def configure(app, norun=False, **oauthspecs):
    """Configures the data custodian to provide an `oauth` server.

    Args:
        app (flask.Flask): flask application to register the grants to.
    """
    global require_oauth
    require_oauth = ResourceProtector()

    if norun:
        # Everything else we do for oauth initialization uses the database, which
        # is not necessarily available when norun is enabled.
        return

    # Initialize the global variables for the oauth server.
    global query_client, save_token, authorization
    query_client = create_query_client_func(db.session, OAuth2Client)
    save_token = create_save_token_func(db.session, OAuth2Token)

    # Before any database initialization is done, we need to update the user
    # model to have the necessary fields.
    configure_user_model(oauthspecs.get("attributes", []))

    # Initialize the database tables and ORM.
    @app.before_first_request
    def create_tables():
        db.create_all()

    # Before we initialize the database from the application specifications, we
    # need to insert them into the application config.
    app.config.update(oauthspecs.get("db", {}))
    if "refresh-token" in oauthspecs.get("grants", []):
        app.config["OAUTH2_REFRESH_TOKEN_GENERATOR"] = True
    db.init_app(app)

    dburi = oauthspecs["db"]["SQLALCHEMY_DATABASE_URI"]
    try:
        _create_db(dburi)
    except:
        log.exception("Creating oauth database.")
        sys.exit(2)

    # Next, initialize the OAuth2 server for the Flask application.
    authorization = AuthorizationServer(query_client=query_client, save_token=save_token)
    authorization.init_app(app)

    global jwt_config, scopes
    jwt_config = oauthspecs.get("jwt", {})
    scopes.update(oauthspecs.get("scopes", {}))

    # Support all the grants listed in the application specification.
    supported = {
        "implicit": grants.ImplicitGrant,
        "client-credentials": grants.ClientCredentialsGrant,
        "authorization-code": OAuthAuthorizationCodeGrant,
        "refresh-token": RefreshTokenGrant,
        "openid-implicit": OpenIDImplicitGrant,
        "openid-hybrid": OpenIDHybridGrant
    }
    for grant_kind in oauthspecs.get("grants", []):
        if grant_kind in supported:
            authorization.register_grant(supported[grant_kind])
        else:
            log.warning(f"`{grant_kind}` is not a supported grant type.")

    if "openid" in oauthspecs.get("grants", []):
        authorization.register_grant(OIDCAuthorizationCodeGrant, [
            OpenIDCode(required_nonce=True),
        ])

    if oauthspecs.get("revocable", True):
        revocation_cls = create_revocation_endpoint(db.session, OAuth2Token)
        authorization.register_endpoint(revocation_cls)

    # Resource protection is always supported out of the box so that the oauth
    # protection for endpoints elsewhere in the application function correctly.
    bearer_cls = create_bearer_token_validator(db.session, OAuth2Token)
    require_oauth.register_token_validator(bearer_cls())
