"""Implements a client object that can interact with data custodian OAuth using
DID-based authentication (instead of needing a user to authenticate with
traditional username/password).
"""
from authlib.integrations.requests_client import OAuth2Session, OAuth2Auth
from authlib.common.urls import add_params_to_uri

from datacustodian.consent.auth import get_signed_headers

def add_didauth(did, _agent):
    """Creates a compliance hook that signs the request headers using `didauth`.

    Args:
        did (str): DID associated with the public key to match the signature.
        agent (str): name of the agent configured with the application that
            has access to signing functionality.
    """

    def signer(uri, headers, body):
        """Signs the specified request headers using `didauth` specification.

        Returns:
            tuple: `(uri, headers, body)` with any updates applied.
        """
        _uri = add_params_to_uri(uri, [("did", did)])
        signed = get_signed_headers(_agent, _uri, None, headers)
        return _uri, signed, body

    return signer


def get_session(did, agent, **kwargs):
    """Returns a :class:`requests.Session` object to interact with a data-custodian
    OAuth server using DID-based authentication.

    .. note:: The session object returned is actually a :class:`OAuth2Session`
        object.

    Args:
        did (str): DID associated with the public key to match the signature.
        agent (str): name of the agent configured with the application that
            has access to signing functionality.
        kwargs (dict): keyword arguments passed on the :class:`OAuth2Session`
            object.
    """
    result = OAuth2Session(**kwargs)
    signer = add_didauth(did, agent)
    result.register_compliance_hook("protected_request", signer)
    return result
