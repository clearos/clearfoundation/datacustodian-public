"""This module implements database models for the SQLAlchemy ORM.
"""
import time
import json
from flask_sqlalchemy import SQLAlchemy
from authlib.flask.oauth2.sqla import (
    OAuth2ClientMixin,
    OAuth2AuthorizationCodeMixin,
    OAuth2TokenMixin,
    OIDCAuthorizationCodeMixin
)

db = SQLAlchemy()
"""flask_sqlalchemy.SQLAlchemy: database ORM client to connect to the oauth
database in postgres.
"""

class User(db.Model):
    did = db.Column(db.String(32), primary_key=True)

    def __str__(self):
        return self.did

    def get_user_id(self):
        """Returns the internal database id of the user, which is the DID for
        data custodian.
        """
        return self.did

strings = ['name', 'given_name', 'family_name', 'middle_name', 'nickname',
           'preferred_username', 'profile', 'picture', 'website', 'email',
           'gender', 'birthdate', 'zoneinfo', 'locale',
           'phone_number']
attr_type_map = {
    'email_verified': (db.Boolean, {}),
    'phone_number_verified': (db.Boolean, {}),
    'updated_at': (db.Numeric, {}),
    'address': (db.JSON, {})
}
"""dict: keys are attribute names, values are `tuple` of arguments that can
be given to :class:`db.Column`.
"""
for field in strings:
    attr_type_map[field] = (db.String(50), {})

def address_to_string(address):
    """Converts a JSON address `dict` following
    https://openid.net/specs/openid-connect-core-1_0.html#AddressClaim
    to a string that can be stored in the database.
    """
    return json.dumps(address)

def string_to_address(straddr):
    """Deserializes the address from database back to a `dict`.
    """
    return json.loads(straddrs)

def configure_user_model(attributes):
    """Adds the necessary attributes to :class:`User` so that the appropriate
    columns are created in the database tables.

    Args:
        attributes (list): of `str` attribute names supported in OpenID claims.
    """
    for field in attributes:
        if field in attr_type_map and not hasattr(User, field):
            colargs, colkwargs = attr_type_map[field]
            if not isinstance(colargs, (tuple, list)):
                colargs = (colargs,)
            setattr(User, field, db.Column(*colargs, **colkwargs))

class OAuth2Client(db.Model, OAuth2ClientMixin):
    __tablename__ = 'oauth2_client'

    did = db.Column(db.String, primary_key=True)
    user_id = db.Column(
        db.String(32), db.ForeignKey('user.did', ondelete='CASCADE'))
    user = db.relationship('User')


class OAuth2AuthorizationCode(db.Model, OAuth2AuthorizationCodeMixin):
    __tablename__ = 'oauth2_code'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.String(32), db.ForeignKey('user.did', ondelete='CASCADE'))
    user = db.relationship('User')


class OIDCAuthorizationCode(db.Model, OIDCAuthorizationCodeMixin):
    __tablename__ = 'oauth2_oidc_code'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.String(32), db.ForeignKey('user.did', ondelete='CASCADE'))
    user = db.relationship('User')


class OAuth2Token(db.Model, OAuth2TokenMixin):
    __tablename__ = 'oauth2_token'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.String(32), db.ForeignKey('user.did', ondelete='CASCADE'))
    user = db.relationship('User')

    def is_refresh_token_active(self):
        if self.revoked:
            return False
        expires_at = self.issued_at + self.expires_in * 2
        return expires_at >= time.time()
