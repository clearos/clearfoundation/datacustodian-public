"""This module exposes functions to implement the endpoint calls in the REST
API.
"""
from authlib.oauth2 import OAuth2Error
from authlib.flask.oauth2 import current_token
from flask import jsonify
from werkzeug.security import gen_salt
import logging

from .models import db, User, OAuth2Client
from . import authorization, filter_user_fields

log = logging.getLogger(__name__)

def revoke(**kwargs):
    """Logs the user out and removes any active tokens.
    """
    return server.create_endpoint_response(MyRevocationEndpoint.ENDPOINT_NAME)

def register(_data=None, **kwargs):
    """Register a new user in the OAuth system of this datacustodian.

    Args:
        _data (dict): key-value pairs corresponding to the OpenIDUser model.
    """
    did = _data["did"]
    user = User.query.filter_by(did=did).first()
    if not user:
        user = User(**_data)
        db.session.add(user)
        db.session.commit()
        log.info(f"Committed user {_data} to `oauth` database.")
    else:
        log.info(f"User {_data} already exists in the database.")

    return {
        "success": True,
        "did": did
    }

def client_create(_params=None, _data=None, **kwargs):
    """Creates a new client for the specified user.

    Args:
        _data (dict): key-value pairs corresponding to the OAuth2Client model.
    """
    did = _params["user_did"]
    client = OAuth2Client.query.filter_by(did=_data["did"]).first()
    if not client:
        client = OAuth2Client(**_data)
        client.user_id = did
        client.client_id = gen_salt(24)
        if client.token_endpoint_auth_method == 'none':
            client.client_secret = ''
        else:
            client.client_secret = gen_salt(48)
        db.session.add(client)
        db.session.commit()
        log.info(f"Committed client {client.client_id} with did={client.did}.")
    else:
        log.info(f"Client with did={client.did} already exists.")

    return {
        "client_id": client.client_id,
        "client_secret": client.client_secret
    }

def client_ls(_params=None, **kwargs):
    """Lists all the registered clients for the user.
    """
    did = _params["user_did"]
    return {
        "clients": OAuth2Client.query.filter_by(user_id=did).all(),
        "did": did
    }

def authorize(_params=None, **kwargs):
    """Authorize a client to access user resources.
    """
    did = _params["did"]
    user = User.query.filter_by(did=did).first()
    return authorization.create_authorization_response(grant_user=user)

def validate(_request=None, **kwargs):
    """Validate a consent request for a user. This is equivalent to asking the
    user to confirm the grant.
    """
    did = _request.headers["keyId"]
    try:
        user = User.query.filter_by(did=did).first()
        grant = authorization.validate_consent_request(end_user=user)
    except OAuth2Error as error:
        return jsonify(dict(error.get_body()))
    return grant

def issue_token(**kwargs):
    """Issues a OAuth token for the client to access user resources.
    """
    return authorization.create_token_response()

def profile(**kwargs):
    """Return the user profile associated with the current token of the login
    request.
    """
    user = current_token.user
    attrs = filter_user_fields(user, current_token.scope)
    return jsonify(attrs)

def scopes_ls(**kwargs):
    """Returns a list of the scopes configured for this application; taken directly
    from the application specs.
    """
    from datacustodian.settings import app_specs
    result = []
    for name, details in app_specs["oauth"].get("scopes", {}).items():
        d = {"name": name}
        d.update(details)
        result.append(d)

    return {
        "scopes": result
    }
