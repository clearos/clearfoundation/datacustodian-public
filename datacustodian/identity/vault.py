"""Manages the password vault for as many users as this data custodian
supports. Each user's passwords are encrypted with a different derived key
that the data custodian controls.
"""
from fnmatch import fnmatch
import logging
import sys
import datetime
from werkzeug.exceptions import BadRequest, Conflict
from werkzeug.security import gen_salt

from .schema import Schema, And, Optional, Or, Use

from datacustodian.crypto import derived_keypair, anon_crypt_message, anon_decrypt_message, bytes_to_b64, b64_to_bytes
from datacustodian.db import dblock, dbget, dbupdate, dbdel

log = logging.getLogger(__name__)

databases = None
"""attrdict.AttrDict: databases for application, acl and fido; this variable is
set during :func:`configure`.
"""
agent_name = None
"""str: name of the agent to use for cryptographic operations.
"""

def _parse_date(datestr):
    return datetime.datetime.strptime(datestr, "%Y-%m-%d %H:%M:%S.%f")

schemas = {
    "credential": Schema({"user": And(str, len), "password": And(str, len)}),
    "address": Schema({
        "addressLocality": And(str, len),
        "addressRegion": And(str, len),
        "postalCode": And(str, len),
        Or("streetAddress", "postOfficeBoxNumber", only_one=True): str,
        Optional("addressCountry"): str,
        Optional("title"): str,
        Optional("firstName"): str,
        Optional("lastName"): str,
        Optional("middleName"): str,
        Optional("company"): str
    }),
    "creditCard": Schema({
        "name": And(str, len),
        "expirationMonth": And(str, len),
        "expirationYear": And(str, len),
        "cvv": And(str, len),
        "number": And(str, len),
        Optional("cardType"): str
    }),
    "secureNote": Schema({
        Optional("created"): Optional(Or(None, And(str, Use(_parse_date)))),
        "note": str,
        Optional("version"): Optional(Or(None, Use(int))),
        "title": And(str, len)
    })
}
"""dict: keys are category kinds in :data:`categories`; values are :class:`schema.Schema`
objects use to validate that a request for a certain kind matches standard schemas.
"""

def _mask_str(s, n, front=True, maxlen=20):
    """Masks the given string `s` by converting all but the first (last) `n`
    characters to `*`.

    Args:
        s (str): string to mask.
        n (int): number of characters to retain in plaintext.
        front (bool): when True, mask the *front* characters of the string (so
            the last `n` characters are plaintext); otherwise, mask the characters
            at the *back* so that the first `n` are plain.
        maxlen (int): maximum length of the masked string to return.
    """
    nmax = len(s) if len(s) > maxlen else maxlen
    if front:
        part = s[-n:] if len(s) > n else s
        return "{{0:*>{0}}}".format(nmax).format(part)
    else:
        part = s[0:n] if len(s) > n else s
        return "{{0:*<{0}}}".format(nmax).format(part)

def _catkey_credential(fieldset):
    users = [v for k, v in fieldset["fields"].items() if k == "user"]
    if len(users) == 1:
        return users[0]
    else:
        return "<unknown>"

def _catkey_address(fieldset):
    if "streetAddress" in fieldset["fields"]:
        return fieldset["fields"]["streetAddress"]
    elif "postOfficeBoxNumber" in fieldset["fields"]:
        return fieldset["fields"]["postOfficeBoxNumber"]
    elif "addressLocality" in fieldset["fields"]:
        return f"{fieldset['fields']['addressLocality']}, {fieldset['fields'].get('postalCode', '<no zip code>')}"

def _catkey_creditcard(fieldset):
    return fieldset["fields"]["number"]

def _catkey_securenote(fieldset):
    return f"{fieldset['fields']['title']}:{fieldset['fields']['version']}"


categories = {
    "credential": _catkey_credential,
    "address": _catkey_address,
    "creditCard": _catkey_creditcard,
    "secureNote": _catkey_securenote
}
"""dict: keys are category kinds supported by the vault. Values are functions
to return the field key based on the category and field values.
"""


def configure(dbs, **vaultargs):
    """Configures the vault databases and keys.
    """
    global databases, agent_name, vault_url
    databases = dbs
    agent_name = vaultargs["agent"]
    vault_url = vaultargs["url"]


def supported_schemas(**kwargs):
    """Returns a list of supported schemas in JSON.
    """
    result = {}
    for n, s in schemas.items():
        result[n] = s.json_schema(f"{vault_url}")

    return result

def get_affected_fields(kind, did, hint_key):
    """Returns a list of the affected fields for the given hint key, assuming
    the user were to approve decryption.

    Args:
        kind (str): of the supported :data:`categories`.
        did (str): DID of the user to simulate sensitive data retrieval for.
        hint_key (str): the dictionary key returned by :func:`get_generic_hints` that
            values should be simulated for.
    """
    src, catkey = hint_key.split("://")
    dbid = f"{kind}/{did}/{src}"
    result = []
    doc = dbget("vault", dbid)
    if doc is not None:
        result = list(doc[catkey]["fields"].keys())

    return set(result)

def get_generic_decrypted(kind, did, hint_key, as_dict=False):
    """Returns the database document for the user's data, and decrypts it.

    Args:
        kind (str): of the supported :data:`categories`.
        did (str): DID of the user to retrieve sensitive data for.
        hint_key (str): the dictionary key returned by :func:`get_generic_hints` that
            values should be decrypted for.
    """
    src, catkey = hint_key.split("://")
    dbid = f"{kind}/{did}/{src}"
    result, index = {}, None
    doc = dbget("vault", dbid)
    if doc is not None:
        log.debug(f"Looking for {catkey} in {doc.keys()}.")
        result = doc.get(catkey, {})
        index = doc.get("index")

    if as_dict:
        fields = {}
    else:
        fields = []

    if index is not None:
        vk, pk, sk = derived_keypair(agent_name, context=dbid, keypath=str(index))
        log.info(f"Decrypting vault entry {catkey}@{dbid}.")
    else:
        pk, sk = None, None

    for name, value in result.get("fields", {}).items():
        if sk is not None:
            valbytes = b64_to_bytes(value)
            decval = anon_decrypt_message(valbytes, pk, sk).decode()
        else:
            decval = value

        if as_dict:
            fields[name] = decval
        else:
            fields.append({
                "name": name,
                "value": decval
            })

    result["fields"] = fields

    return result


def _doc_hints(doc, filter):
    """Filters field sets available on a vault document.
    """
    result = {}
    if doc.exists:
        for catkey, fieldset in doc.items():
            if catkey in ["index", "_id", "_rev"]:
                # This is a key that is for management of the vault.
                continue

            if fnmatch(fieldset["hint"], filter) or fnmatch(catkey, filter):
                result[catkey] = fieldset["hint"]

    return result

def get_generic_hints(kind, did, key, filter='*'):
    """Returns a list of available field names and value hints.

    Args:
        kind (str): of the supported :data:`categories`.
        did (str): DID of the user to retrieve sensitive data for.
        key (str): unique database identifier to retrieve.
        filter (str): filters possible matches using the `name` and `hint`
            entries of each field.
    """
    # First, get a list of global matches to the search.
    glob_id = f"{kind}/{did}/global"
    dbid = f"{kind}/{did}/{key}"
    result = {}

    doc = dbget("vault", glob_id)
    result.update({f"global://{k}": v for k, v in _doc_hints(doc, filter).items()})

    doc = dbget("vault", dbid)
    result.update({f"{key}://{k}": v for k, v in _doc_hints(doc, filter).items()})

    log.debug(f"Hints queried at {glob_id} and {dbid}.")

    return result

def get_user_index(kind, did, key, increment=False):
    """Retrieves the user metadata index from the vault database.

    Returns:
        dict: with keys ['index'].
    """
    result = None
    dbid = f"{kind}/{did}"
    updating = False or increment

    # We want to be careful here. Because we have global locks for all database updates, we only
    # want to secure a lock if updates will actually be performed. If the keys and index are up
    # to date, just return the doc.
    doc = dbget("vault", dbid)
    if doc is not None:
        updating = increment or "index" not in doc or "keys" not in doc or (key is not None and key not in doc["keys"])
        if not updating:
            return doc

    with dblock("vault", dbid) as doc:
        if "index" in doc:
            if increment:
                doc["index"] += 1
        else:
            doc["index"] = 0

        # Keep track of all the keys the user has in their vault.
        if "keys" not in doc:
            doc["keys"] = []
        if key is not None and key not in doc["keys"]:
            doc["keys"].append(key)

        result = doc

    return result


def bulk_key_update(did, registry):
    """Updates the users global key registry in a single bulk operation.

    Args:
        registry (dict): keys are `kind` categories, values are a list of keys to update
            the user's key registry with.
    """
    for kind, keys in registry.items():
        dbid = f"{kind}/{did}"

        with dblock("vault", dbid) as doc:
            # Keep track of all the keys the user has in their vault.
            if "keys" not in doc:
                doc["keys"] = []

            unique = set(doc["keys"]) | set(keys)
            doc["keys"] = list(unique)


def _process_special(kind, doc, validated):
    """Performs special processing for specific key-value pairs of a data entry.

    Args:
        kind (str): one of the supported schema kinds in :data:`schemas`.
        doc (dict): the existing database document.
        validated (dict): key-value pairs that have been validated for the given
            schema.
    """
    for name, value in validated.items():
        if kind == "secureNote":
            if name == "version":
                if doc.get("note") != validated.get("note"):
                    if validated.get("version") is not None:
                        validated[name] = validated["version"]
                    else:
                        validated[name] = doc.get("version", 0) + 1
            elif name == "created":
                if doc.get("created") is None and validated.get("created") is None:
                    validated[name] = str(datetime.datetime.utcnow())

    return validated


def _save_single(kind, fieldset, doc, pk):
    """Conforms a single field set and then adds it to the database document.
    """
    # Validate the schema if it is using a known kind. TODO: switch from list
    # of dicts to key-value pair and then validate.
    if kind not in schemas:
        raise BadRequest("The category you specified is not supported by the vault.")

    inverse = {f["name"]: f["value"] for f in fieldset["fields"]}
    try:
        validated = schemas[kind].validate(inverse)
    except Exception as e:
        raise BadRequest(f"The store request did not pass schema validation: {e}.")

    log.debug(f"Validation produced {len(validated)} fields from {len(inverse)} fields.")
    if kind == "secureNote":
        if "created" not in validated:
            validated["created"] = None
        if "version" not in validated:
            validated["version"] = 0

    # Do any special processing of the key-value pairs; for example incrementing
    # the version number on secure notes if the contents have changed.
    fieldset["fields"] = _process_special(kind, doc, validated)

    # For bulk imports, we want to help the user not have duplicate entries. Even if we can't see
    # the actual user name because it's encrypted, we can still use existing keys if the encrypted
    # value is the same. Make sure we compile the dedupe key before our second layer of encryption.
    dedupe_key = fieldset["hint"]
    if "dedupe" not in doc:
        doc["dedupe"] = {}
    if dedupe_key in doc["dedupe"]:
        catkey = doc["dedupe"][dedupe_key]
        log.debug(f"Using dedupe key {catkey} for vault update.")
    else:
        catkey = fieldset.get("key", gen_salt(16))
        doc["dedupe"][dedupe_key] = catkey
        log.debug(f"Using database key {catkey} for new vault entry.")

    if catkey not in doc:
        doc[catkey] = fieldset.copy()
        doc[catkey]["fields"] = {}

    for name, value in validated.items():
        # Make a copy of the field and encrypt its value if a key was supplied.
        if pk is not None:
            encval = bytes_to_b64(anon_crypt_message(str(value).encode(), pk))
        else:
            encval = str(value)
        doc[catkey]["fields"][name] = encval

    log.debug(f"Stored single vault entry with {len(doc[catkey]['fields'])} fields.")

    return catkey


def delete_generic(kind, did, key, hint_key=None):
    """Deletes sensitive data from the user database.

    .. note:: To delete a `global` entry, set the key to `global`.

    Args:
        kind (str): of the supported :data:`categories`.
        did (str): DID of the user to retrieve sensitive data for.
        key (str): unique database identifier to retrieve.
        hint_key (str): specific item to delete; if not specified,
            nothing is deleted.
    """
    dbid = f"{kind}/{did}/{key}"
    with dblock("vault", dbid) as doc:
        if hint_key in doc:
            log.info(f"Deleting sensitive data {hint_key} under {dbid}.")
            del doc[hint_key]


def save_generic(kind, did, key, data, bulk=False):
    """Saves sensitive user data to the database in encrypted format.

    Args:
        kind (str): of the supported :data:`categories`.
        did (str): DID of the user to retrieve sensitive data for.
        key (str): unique database identifier to retrieve.
        data (dict): that follows the format of the `WebsiteFormStore` model.
        bulk (bool): when True, don't use a global database lock for the update; assume that the
            update is exclusive for those keys.
    """
    global_sets, keyed_sets = [], []

    for fieldset in data:
        if fieldset["global"]:
            global_sets.append(fieldset)
        else:
            keyed_sets.append(fieldset)

    dbcall = dbupdate if bulk else dblock

    dbid = f"{kind}/{did}/{key}"
    catkeys = []
    if len(keyed_sets) > 0:
        with dbcall("vault", dbid, 300) as doc:
            if not bulk:
                if "index" not in doc:
                    get_user_index(kind, did, key, increment=False)

            # vk, pk, sk = derived_keypair(agent_name, context=dbid, keypath=str(index))
            # del sk
            pk = None

            # Update the field information included, don't clobber other
            # entries that are already in the doc.
            for fieldset in keyed_sets:
                catkey = _save_single(kind, fieldset, doc, pk)
                catkeys.append(catkey)
                log.info(f"Stored {catkey} document to user {did} vault.")

    if len(global_sets) == 0:
        return catkeys

    dbid = f"{kind}/{did}/global"
    with dbcall("vault", dbid, 300) as doc:
        if not bulk:
            if "index" not in doc:
                get_user_index(kind, did, "global", increment=False)

        # vk, pk, sk = derived_keypair(agent_name, context=dbid, keypath=str(index))
        # del sk
        pk = None

        for fieldset in global_sets:
            catkey = _save_single(kind, fieldset, doc, pk)
            catkeys.append(catkey)
            log.info(f"Stored {catkey} global document to user {did} vault.")

    return catkeys


_category_map = {
    "credential": "user",
    "address": "address",
    "creditCard": "last4Digits",
    "secureNote": "title:version"
}

def list_vault_keys(did):
    """Lists all the keys stored in the database for a given DID.
    """
    skip_keys = ("_id", "_rev", "index", "verkey", "dedupe")
    result = {}
    for kind in categories:
        index = get_user_index(kind, did, None)
        result[kind] = []
        for key in index.get("keys", []):
            kdict = {"url":  key}
            dbid = f"{kind}/{did}/{key}"
            doc = dbget("vault", dbid)
            if doc is not None:
                kdict["entries"] = [{
                    "key": k,
                    "hint": v["hint"]
                }
                    for k, v in doc.items() 
                    if k not in skip_keys and hasattr(v, "__iter__") and "hint" in v
                ]
            else:
                kdict["entries"] = []

            result[kind].append(kdict)

    return result
