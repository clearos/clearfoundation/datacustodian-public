"""Implements a FIDO server for credential registration and authentication. The
credentials are stored for each user in the `couchdb` for the data custodian.
This implementation follows the `webauthn` specification closely: https://www.w3.org/TR/webauthn
"""
import asyncio
from fido2.client import ClientData
from fido2.webauthn import PublicKeyCredentialRpEntity, UserVerificationRequirement, AuthenticatorAttachment
from fido2.server import Fido2Server
from fido2.ctap2 import AttestationObject, AuthenticatorData, AttestedCredentialData
import cbor
from urllib.parse import urlparse
from flask import Response, stream_with_context
import logging
import functools
import qrcode
import time
import six
import threading
from io import BytesIO
from datetime import datetime
from werkzeug.exceptions import BadRequest, RequestTimeout, InternalServerError, Unauthorized, NotFound, Conflict
from werkzeug.security import gen_salt
from attrdict import AttrDict
import concurrent.futures

from datacustodian.utility import import_fqn
from datacustodian.db import get_view, dblock, dbget, dbdel, dbupdate, memstore, memget
from datacustodian.crypto import bytes_to_b64, b64_to_bytes
from datacustodian.rpc import get_manager, check_rpc_obj
from clear.push import configure as configure_push, send
from .did import configure as configure_did, get_did_user
from .vault import (configure as configure_vault, get_generic_hints, get_generic_decrypted,
                    save_generic, get_affected_fields, list_vault_keys, delete_generic, bulk_key_update)

log = logging.getLogger(__name__)

rp = None
"""fido2.server.RelyingParty: as per the FIDO specs, this is an implementation
of the `RelyingParty` interface for FIDO2.
"""
server = None
"""fido2.server.Fido2Server: object that implements the server interface with
methods for registration and authentication that start and then complete (by
validating signatures) the requests.
"""
user_verification = UserVerificationRequirement.PREFERRED
"""fido2.server.USER_VERIFICATION: default enum selection for user verification
type in the FIDO server. One of `discouraged`, `preferred`, or `required`.
"""
authenticator_attachment = None
"""fido2.server.AUTHENTICATOR_ATTACHMENT: default enum selection for authenticator
attachment. One of `platform`, `cross_platform` or `None`.
"""
databases = None
"""attrdict.AttrDict: databases for application, acl and fido; this variable is
set during :func:`configure`.
"""
wait_time = 60
"""int: number of seconds to wait for a user to respond to a push notification
for FIDO authentication.
"""
pair_timeout = 120
"""int: number of seconds to wait for the user to scan a QR code to pair their
browser plug-in with their mobile phone.
"""
push_enabled = None
"""bool: when True, a `sender_id` and API key were configured for sending push
notifications to users; otherwise, the system is running in testing mode.
"""
max_credential_count = None
"""int: maximum number of credentials that a user can register at the home server.
Set to `None` to allow unlimited number of registrations.
"""

def configure(servspecs, **fidoargs):
    """Configures the FIDO server and relying party based on the application
    specifications.

    Args:
        servspecs (dict): server specifications that include the host name that
            this data custodian is bound to.
    """
    global rp, server, databases
    # First configure the server name and icon from application specifications.
    rp = PublicKeyCredentialRpEntity(fidoargs["host"], 
                                     fidoargs.get("name", "Data Custodian FIDO Server"), 
                                     fidoargs.get("icon"))

    # Grab a reference to the databases; this happens now because they wouldn't
    # be initialized on import of the module.
    from datacustodian.db import databases as dcdbs
    databases = dcdbs

    # Next, create a server object to handle verification of signatures etc.
    # according to the FIDO specification.
    verify_origin = None
    if "origin_verifier" in fidoargs:
        fqn = fidoargs["origin_verifier"]
        _, verify_origin = import_fqn(fqn)

    server = Fido2Server(rp,
                         attestation=fidoargs.get("attestation", "none"),
                         verify_origin=verify_origin)

    # Finally, get the default enum values for registration/authentication calls
    # so that we can reuse them later.
    global user_verification, authenticator_attachment, max_credential_count
    if "user_verification" in fidoargs:
        user_verification = UserVerificationRequirement(fidoargs["user_verification"])
    if "authenticator_attachment" in fidoargs:
        authenticator_attachment = AuthenticatorAttachment(fidoargs["authenticator_attachment"])
    if "apk_hashes" in fidoargs:
        from fido2.rpid import configure as configure_rpid
        configure_rpid(fidoargs["apk_hashes"])
    if "max_credential_count" in fidoargs:
        max_credential_count = fidoargs["max_credential_count"]

    # Also, configure the push notification system.
    global wait_time, pair_timeout, push_enabled
    if "push" in fidoargs:
        configure_push(**fidoargs["push"])
        push_enabled = True
    if "wait_time" in fidoargs:
        wait_time = fidoargs["wait_time"]
        log.debug(f"Set wait time for push notifications to {wait_time} seconds.")
    if "pair_timeout" in fidoargs:
        pair_timeout = fidoargs["pair_timeout"]
        log.debug(f"Set QR pairing timeout to {pair_timeout} seconds.")

    # Finally, configure the password manager vault.
    configure_vault(dcdbs, **fidoargs.get("vault", {}))
    # Configure the DID registration components.
    configure_did(dcdbs)


def start():
    """Starts FIDO-based resources once all dependent services (basically the
    cloud agents) have been initialized.
    """
    # Configure the push consumer process queue.
    from .push import start as start_push
    start_push()


def shutdown():
    """Shuts down the remote synchronization manager needed for FIDO.
    """
    from .push import shutdown as shutdown_push
    shutdown_push()


def _get_credentials(did):
    """Retrieves a list of stored credentials (if they exist) for the given DID.
    """
    credentials, raw_credentials = [], {}
    doc = dbget("fido", did)
    if doc is not None:
        raw_credentials = doc.get("credentials", {})
        log.debug(f"Found {len(raw_credentials)} credentials during lookup for {did}.")
    else:
        log.debug(f"No credentials stored in db for user {did}.")

    # Build the credentials from their raw format.
    for credential_id, cred_data in raw_credentials.items():
        log.debug(f"Found credential {credential_id} in database for {did}.")
        credential, _rest = AttestedCredentialData.unpack_from(b64_to_bytes(cred_data))
        credentials.append(credential)

    return credentials

def _state_dbid(did):
    return  "{did}/state"

def _get_state(challenge):
    """Returns any stored state for the DID from the database. The specified
    challenge value is used to find the DID and stored state.

    Returns:
        tuple: `(did, state)` with the found DID and the state dictionary. See
        :func:`_store_state`.
    """
    # Find the user/DID that was given the challenge in this request.
    did = memget("fido", challenge)

    # Now, lookup the database entry so we can get the state information.
    state, extras = {}, None
    dbid = _state_dbid(did)
    doc = dbget("fido", dbid)
    if doc is None or challenge not in doc["challenges"]:
        raise NotFound("Requested resource does not exist; invalid challenge value.")

    cached = doc["challenges"][challenge]
    state["challenge"] = challenge
    state["user_verification"] = cached["user_verification"]
    extras = cached["extras"]
    
    log.debug(f"Found state {state} from database for {did}.")

    return did, state, extras


def _store_state(did, state, extras=None):
    """Stores the given state to database for the DID.

    Args:
        did (str): decentralized identifier to store under.
        state (dict): with keys `challenge` and `user_verification`.
        extras (dict): extra values to associate with the user state.
    """
    dbid = _state_dbid(did)
    with dblock("fido", dbid, 60) as doc:
        if "challenge" not in doc:
            doc["challenges"] = {}
        doc["challenges"][state["challenge"]] = {
            "user_verification": state["user_verification"],
            "extras": extras
        }
    log.debug(f"{state} saved in FIDO database for {did}.")

    # For stability, we have to use the in-memory cache for mapping challenges to DIDs because
    # the couchdb only has eventual consistency and users may be really quick in responding to 
    # FIDO challenges.
    memstore("fido", state["challenge"], did, 30)


def _delete_state(did, challenge):
    """Removes a challenge state entry for the DID.
    """
    dbid = _state_dbid(did)
    dbdel("fido", dbid)
    log.debug(f"State deleted for {challenge} from FIDO database for {did}.")


def register_begin_options(**kwargs):
    """Returns the registration options for CORS and credentials.
    """
    headers = [
        ("Access-Control-Allow-Headers", "access-control-allow-origin"),
        ("Access-Control-Allow-Origin", "*"),
        ("Access-Control-Allow-Methods", "post")
    ]
    return Response(headers=headers)


def register_begin(_data=None, _request=None, _params=None, **kwargs):
    """Handles the initial request to start registration of an authenticator
    for a given user.

    Args:
        _data (dict): user data description following the schema of `FIDOUserData`
            model.
    """
    did = _data["did"]
    extras = {k: v for k, v in _params.items() if k != "did"}
    credentials = None
    """list: of credentials to be sent in the *exclude* list.
    """

    # Only send the credentials in the creation options if the home server limits the 
    # number that can be registered.
    if max_credential_count is not None:
        _credentials = _get_credentials(did)
        if len(credentials) >= max_credential_count:
            credentials = _credentials

    user_data = get_did_user(did)
    registration_data, state = server.register_begin(user_data, credentials, user_verification=user_verification)

    # Since the requester may not have cookies enabled, save the challenge to
    # the database so we can access it on the next request.
    _store_state(did, state, extras)
    log.debug(f"Registration started with data {registration_data}.")

    # Prepare the response as an octet stream; otherwise flask tries to  encode
    # it as JSON.
    result = cbor.dumps(registration_data)
    headers = [
        ("Content-Length", str(len(result))),
        ("Access-Control-Allow-Origin", "*")
    ]
    return Response(result,
                    mimetype='application/octet-stream',
                    headers=headers,
                    direct_passthrough=True)

def register_end_options(**kwargs):
    """Returns the registration options for CORS and credentials.
    """
    headers = [
        ("Access-Control-Allow-Headers", "access-control-allow-origin,content-type"),
        ("Access-Control-Allow-Origin", "*"),
        ("Access-Control-Allow-Methods", "post")
    ]
    return Response(headers=headers)

def register_end(_request=None, **kwargs):
    """Handles the `/complete` request to finalize registration of a credential
    for a user-authenticator pair.
    """
    data = cbor.loads(_request.get_data())

    # These next lines follow the spec closely; it defines special data formats
    # where each bit has specific meaning to keep the data as small as possible.
    client_data = ClientData(data['clientDataJSON'])
    att_obj = AttestationObject(data['attestationObject'])
    log.debug(f"Registration complete triggered with client data: {client_data}.")
    log.debug(f"Attestation accompanying registration complete is {att_obj}.")

    # See if we have a valid state stored for the challenge.
    did, state, _ = _get_state(client_data.get("challenge"))

    if "localhost" in client_data.data["origin"] and "https://" not in client_data.data["origin"]:
        # This is a hack for local testing to spoof a secure connection.
        client_data.data["origin"] = client_data.data["origin"].replace("http://", "https://")

    log.debug(f"Registration complete triggered with client data: {client_data}.")
    log.debug(f"Attestation accompanying registration complete is {att_obj}.")
    auth_data = server.register_complete(
        state,
        client_data,
        att_obj
    )

    # Remove this challenge state entry to avoid database bloat
    _delete_state(did, client_data.get("challenge"))

    # Store a base-64 encoded version of the credential for the DID.
    credential_id = bytes_to_b64(auth_data.credential_data.credential_id)
    with dblock("fido", did) as doc:
        if "credentials" not in doc:
            doc["credentials"] = {}

        log.debug(f"Storing credential: {credential_id} for {did}.")
        doc["credentials"][credential_id] = bytes_to_b64(bytes(auth_data.credential_data))

    allcreds = [{
                    "credId": c.credential_id,
                    "publicKey": c.public_key
                } for c in _get_credentials(did)]
    # Because the database only guarantees *eventual* consistency, make sure the newly added 
    # credential is part of the response.
    if auth_data.credential_data.credential_id not in [d["credId"] for d in allcreds]:
        allcreds.append({
            "credId": auth_data.credential_data.credential_id,
            "publicKey": auth_data.credential_data.public_key
        })

    result = cbor.dumps({
        'status': 'OK',
        'credentials': allcreds
    })
    headers = [
        ("Content-Length", str(len(result))),
    ]
    return Response(result,
                    mimetype='application/octet-stream',
                    headers=headers,
                    direct_passthrough=True)


def authenticate_begin_options(**kwargs):
    """Returns the registration options for CORS and credentials.
    """
    headers = [
        ("Access-Control-Allow-Headers", "access-control-allow-origin"),
        ("Access-Control-Allow-Origin", "*"),
        ("Access-Control-Allow-Methods", "post")
    ]
    return Response(headers=headers)


def authenticate_begin(_data=None, **kwargs):
    """Starts an authentication session by returning a challenge to the user.
    """
    did = _data["did"]
    extras = {k: v for k, v in _data.items() if k != "did"}
    credentials = _get_credentials(did)
    if len(credentials) == 0:
        raise NotFound(f"Could not find matching credentials for {did}.")

    auth_data, state = server.authenticate_begin(credentials)
    _store_state(did, state, extras)

    result = cbor.dumps(auth_data)
    headers = [
        ("Content-Length", str(len(result))),
        ("Access-Control-Allow-Origin", "*"),
    ]
    return Response(result,
                    mimetype='application/octet-stream',
                    headers=headers,
                    direct_passthrough=True)


def authenticate_end_options(**kwargs):
    """Returns the registration options for CORS and credentials.
    """
    headers = [
        ("Access-Control-Allow-Headers", "access-control-allow-origin,content-type"),
        ("Access-Control-Allow-Origin", "*"),
        ("Access-Control-Allow-Methods", "post")
    ]
    return Response(headers=headers)


def _handle_auth_extras(extras):
    """Handles the extras *after* the user has already authenticated.

    .. warning:: This function assumes that the user gave permission to access
        all their data!
    """
    manager = get_manager()
    method = extras.get("method")
    log.debug(f"Handling auth extras {extras}.")
    if method == "push":
        # Set the threading Event flag for this particular message, which
        # indicates that the user approved its contents.
        message_id = extras["message_id"]
        event = manager.get_event("push", message_id)
        check_rpc_obj(event, message_id)
        event._callmethod("set")
        return True


def simulate_app(_data=None, **kwargs):
    """Endpoint to simulate the behavior of `authenticate_end` with an actual
    FIDO request where the user authenticates with their device.
    """
    ok = _handle_auth_extras(_data)
    return {
        "success": ok
    }


def authenticate_end(_request=None, **kwargs):
    """Completes an authentication request by validating the user's signature
    on the challenge value.
    """
    data = cbor.loads(_request.get_data())
    credential_id = data["credentialId"]
    client_data = ClientData(data["clientDataJSON"])
    auth_data = AuthenticatorData(data["authenticatorData"])
    signature = data["signature"]
    log.debug(f"Authenticate complete triggered with client data: {client_data}.")
    log.debug(f"Authenticator accompanying registration complete is {auth_data}.")
    log.debug(f"Signature presented is {signature} for {credential_id}.")

    did, state, extras = _get_state(client_data.get("challenge"))
    credentials = _get_credentials(did)
    if len(credentials) == 0:
        raise NotFound(f"Could not locate registered credentials for {did}.")

    if "localhost" in client_data.data["origin"] and "https://" not in client_data.data["origin"]:
        # This is a hack for local testing to spoof a secure connection.
        client_data.data["origin"] = client_data.data["origin"].replace("http://", "https://")

    server.authenticate_complete(
        state,
        credentials,
        credential_id,
        client_data,
        auth_data,
        signature,
    )
    _delete_state(did, client_data.get("challenge"))

    # Now that we know the authentication is okay, let's look at our extras
    # and do anything extra.
    _handle_auth_extras(extras)

    allcreds = _get_credentials(did)
    result = cbor.dumps({
        'status': 'OK',
        'credentials': [{
            "credId": c.credential_id,
            "publicKey": c.public_key
        } for c in allcreds]
    })
    headers = [
        ("Content-Length", str(len(result))),
    ]
    return Response(result,
                    mimetype='application/octet-stream',
                    headers=headers,
                    direct_passthrough=True)


def cleanup_pairings(pair_rows):
    """Cleans the database for orphaned pairings.

    Args:
        pair_rows (list): of `dict` row objects returned by the `pairing_did_view`.
    """
    deleteable = []
    for r in pair_rows:
        if (len(r["value"]) == 0 or
            r["value"]["push_token"] in ["None", "specifications"]):
            deleteable.append(r["id"])

    if len(deleteable) > 0:
        log.info(f"Clearing orphaned pairing entries: {deleteable}.")

    for docid in deleteable:
        dbdel("fido", docid)


def _get_push_tokens(did, plugin_token):
    """Retrieves a list of push tokens associated with the plugin token.

    Args:
        did (str): DID of the user that approved the plug-in for their data.
        plugin_token (str): token received when the plug-in was first connected
            to the user's phone.
    """
    log.debug(f"Query database for pairing by DID={did}; pairing token is {plugin_token}.")
    view = get_view("fido", "pairing_did_view")
    rows = view(key=did)["rows"]
    if len(rows) == 0:
        raise BadRequest("The given plug-in token is not valid.")

    # If the user has multiple devices, we will send a push to each one. The
    # view above will return multiple rows if the same DID was used (and it matches
    # the token of the authorized plugin).
    tokens = [r["value"]["push_token"] for r in rows
              if "plugin_token" in r["value"]
              and r["value"]["plugin_token"] == plugin_token]
    log.debug(f"Found {len(tokens)} valid push tokens: {tokens}.")
    return tokens


def _get_url(url):
    """Gets the `netloc` portion of the URL.
    """
    if url is None:
        raise BadRequest("The specified URL is invalid.")
    if "http" not in url:
        url = f"http://{url}"
    return urlparse(url).netloc

def _send_push(tokens, title, msg, body=None, did=None, wait_for=5.):
    """Sends a push notification to the user to ask for permission for something.
    This function manages the RPC server sychronization aspects of the pushing
    and waiting for the push to be sent.

    Args:
        tokens (list): of device push tokens to send to.
        title (str): title of the push notification.
        msg (str): message to send in the notification.
        body (dict): optional body containing data to send with the push
            notification. Will be encrypted if `did` is not `None`.
        did (str): if the message should be encrypted, the DID of the user that
            notifications are being sent to.
        wait_for (float): number of seconds to wait for the push to be sent.

    Returns:
        tuple: `(success, failed)` where each is a list of :class:`MessageResponse`
        that includes the result of sending the push notification.
    """
    ping = 0.5
    log.debug(f"Sending push notification to user with {body}.")
    success, failed = [], []
    manager = get_manager()
    grant_id = None

    if push_enabled:
        correlation_id = hash(f"{did}:{time.monotonic()}")
        # First, add the push notification details to the manager's queue.
        manager.add_push(correlation_id, did, tokens, title, msg, body=body)
        # Then, query the manager every half second to see if the result is
        # ready.
        check_counter = 0

        while check_counter < int(wait_for/ping):
            time.sleep(ping)
            if manager.push_result_ready(correlation_id)._getvalue() == True:
                success, failed = manager.get_push_result(correlation_id)._getvalue()
                break
            check_counter += 1

        if len(success) == 0 and len(failed) == 0:
            raise RequestTimeout("Server could not send the push notification in time.")
        elif len(success) == 0 and len(failed) > 0:
            raise InternalServerError(f"Sending the push notification failed: {failed[0].status} > {failed[0].description}.")

        # Store the successful notification messages in the database so
        # that we can correlate the FIDO authentication with a message id.
        for response in success:
            with dbupdate("fido", f"push/{response.message_id}") as doc:
                doc["push_sent"] = True
            event = manager.Event()
            manager.create_event("push", response.message_id, event)

        # Now, block the thread until we have a positive response from the
        # user on one of their devices.
        i = 0
        grant_id = None
        while i < wait_time and grant_id is None:
            first = success[0].message_id
            # Block the thread for one second to see if their is a response.
            first_event = manager.get_event("push", first)
            check_rpc_obj(first_event, first)
            first_event._callmethod("wait", (1.,))
            if first_event._callmethod("is_set"):
                grant_id = response.message_id
                log.info(f"Access granted with message {grant_id}.")
            else:
                # See if any of the others were triggered.
                if len(success) > 1:
                    for response in success[1:]:
                        mid = response.message_id
                        push_event = manager.get_event("push", mid)
                        check_rpc_obj(push_event, mid)
                        if push_event._callmethod("is_set"):
                            grant_id = mid
                            log.info(f"Access granted with message {grant_id}.")

            i += 1

        if grant_id is not None:
            # Clean-up the threading events and database entries.
            for response in success:
                mid = response.message_id
                if mid != grant_id:
                    dbdel("fido", f"push/{mid}")
                else:
                    # This is the one the user gave permission on, save
                    # the body they authorized.
                    with dbupdate("fido", f"push/{mid}") as doc:
                        doc["body"] = body
                        doc["did"] = did

                manager.del_event("push", mid)


    return grant_id


def _common_get_credentials(did, url, grant_id):
    """Performs the credential lookup needed by the `/credentials` endpoint
    for either the plug-in or the mobile app. Assumes that authorization has
    already been granted.

    Args:
        did (str): DID of the user to retrieve credentials for.
        url (str): URL to lookup usernames and passwords for.
        grant_id (str): grant identifier that proves release authorization for
            a password.
    """
    # Get a list of all possible matching credentials for the url.
    possible = get_generic_hints("credential", did, url)

    if len(possible) == 1:
        hint_key = next(iter(possible.keys()))

        # Get the actual password now using a call to decrypt.
        actual = get_generic_decrypted("credential", did, hint_key)
        inverted = {e["name"]: e["value"] for e in actual["fields"]}

        return {
            "users": [inverted["user"]],
            "password": inverted["password"],
            "token": None
        }
    elif len(possible) > 1:
        # We need to save the token so that the subsequent request
        # for a single password doesn't need another push.
        pwtoken = gen_salt(48)
        with dbupdate("fido", f"pw/{pwtoken}") as doc:
            doc["created"] = str(datetime.utcnow())
            doc["users"] = list(possible.keys())
            doc["grant_id"] = grant_id

        return {
            "users": list(possible.keys()),
            "password": None,
            "token": pwtoken
        }
    else:
        return {
            "users": [],
            "password": None,
            "token": None
        }

def plugin_get_credentials(_params=None, _loop=None, _request=None, **kwargs):
    """Sends a push request to the user to ask for permission to release login
    credentials for a website. If there is only one matching login, then both
    the username and the password are released. If there are multiple, then only
    the usernames are released along with a challenge token (valid for a few seconds).
    """
    did, token, url = _params["did"], _params["token"], _get_url(_params["url"])

    tokens = _get_push_tokens(did, token)
    if len(tokens) > 0:
        # Construct a body that tells the user exactly what is happening with their
        # data and passwords.
        body = {
            "url": url,
            "fields": ["username", "password"],
            "plugin": token
        }

        # Send a push to the user's phone to ask for permission to connect.
        msg = f"Release login credentials for {url}?"
        log.debug(f"Sending push notification {msg} to user with {body}.")
        grant_id = _send_push(tokens, "Authorize Login", msg, body, did=did)
        if grant_id is None and not push_enabled:
            from uuid import uuid4
            testing_id = str(uuid4())
            grant_id = testing_id
            log.debug("Push notifications not enabled... Login proceeding in "
                      f"testing mode with message_id={testing_id}.")

        if grant_id is not None:
            return _common_get_credentials(did, url, grant_id)
        else:
            raise RequestTimeout("Timeout waiting for user to grant FIDO authorization from push.")
    else:
        # No matching pairings, we can't get permission to use the passwords.
        raise BadRequest("The plug-in token you specified is not paired with any user authorization.")


def mobile_get_credentials(_params=None, **kwargs):
    """Release login credentials for a website. If there is only one matching login, then both
    the username and the password are released. If there are multiple, then only
    the usernames are released along with a challenge token (valid for a few seconds).
    """
    did, url = _params["did"], _get_url(_params["url"])
    return _common_get_credentials(did, url, "didauth")


def _common_get_grantid(did, hint_key, token):
    """Retrieves the `grant_id` connected to `token` that authorizes release
    of the user's password.

    Args:
        did (str): DID of the user who owns the password.
        hint_key (str): URL/user to retrieve a password for.
        token (str): if this follows a call to `/credential`, the lookup token
            that approved the release.

    Returns:
        tuple: `(grant_id, url)` with the `grant_id` connected to `token` and `url`
        is the URL connected to the grant.
    """
    grant_id, expired, url = None, False, None
    dbid = f"pw/{token}"
    doc = dbget("fido", dbid)

    if doc is None or "created" not in doc:
        dbdel("fido", dbid)
        log.debug(f"Supplied password token {token} doesn't exist.")
        raise BadRequest("The supplied token is invalid. Use the *password* token, "
                            "not the *plugin* token.")

    created = doc["created"]
    delta = datetime.utcnow()-datetime.strptime(created, "%Y-%m-%d %H:%M:%S.%f")
    if delta.seconds < 60:
        if hint_key in doc["users"]:
            grant_id = doc["grant_id"]
    else:
        # The token has expired, delete it.
        dbdel("fido", dbid)
        expired = True

    if expired:
        raise BadRequest("The token you gave has expired. Call `/credential` again.")

    if grant_id is not None:
        doc = dbget("fido", f"push/{grant_id}")
        url = doc["body"]["url"]
        if did != doc["did"]:
            log.debug(f"{did} doesn't match password token approval DID: {doc['did']}.")
            raise BadRequest("The DID you specified is not associated with the "
                                "password token.")

    return grant_id, url


def _common_get_password(did, hint_key, url):
    """Retrieves the user password for the `/password` endpoint of the plug-in
    or mobile endpoints.

    Args:
        did (str): DID of the user who owns the password.
        hint_key (str): URL/user to retrieve a password for.
        url (str): URL that the password is being released for.
    """
    if url is not None:
        log.debug(f"Looking up password for {hint_key} and {url}.")
        actual = get_generic_decrypted("credential", did, hint_key)
        inverted = {e["name"]: e["value"] for e in actual["fields"]}

        if "user" in inverted and "password" in inverted:
            return {
                "user": inverted["user"],
                "password": inverted["password"]
            }
        else:
            return {}
    else:
        raise BadRequest(f"The URL {url} is invalid.")


def plugin_autocred(_params=None, **kwargs):
    """Retrieves credentials for a URL using an `autoId` obtained from :func:`plugin_autochange`.
    """
    autoId, url, key, did = _params["autoId"], _params["url"], _params["key"], _params["did"]
    dbid = f"{did}/autochange/{autoId}"
    doc = dbget("fido", dbid)
    if doc is None:
        raise BadRequest(f"Specified autoId is invalid: {autoId} for {did}.")
    if url not in doc["urls"]:
        raise BadRequest(f"Specified URL {url} was not approved with autoId {autoId}.")

    delta = (datetime.utcnow()-datetime.strptime(doc["granted"], "%Y-%m-%d %H:%M:%S.%f")).seconds
    if (delta > 600):
        dbdel("fido", dbid)
        raise BadRequest("The autoId's permission has expired.")

    hint_key = f"{url}://{key}"
    return _common_get_password(did, hint_key, url)


def plugin_autochange(_data=None, **kwargs):
    """Starts an auto-change session, which asks the user for permission for a set of URLs 
    all at once, and then allows the plugin to retrieve multiple passwords afterwards
    without needing additional push notifications.
    """
    did, token = _data["did"], _data["token"]
    urls = _data["urls"]
    grant_id = None

    # Send the user a push notification to request approval of the auto-change
    tokens = _get_push_tokens(did, token)
    if len(tokens) > 0:
        body = {
            "urls": urls,
            "fields": ["username", "password"],
            "plugin": token
        }

        # Send a push to the user's phone to ask for permission to connect.
        msg = f"Approve passwsord auto-changing?"
        grant_id = _send_push(tokens, "Approve Auto-Change", msg, body, did=did)

        if grant_id is not None:
            autoId = gen_salt(32)
            with dblock("fido", f"{did}/autochange/{autoId}", 300) as doc:
                doc["urls"] = urls
                doc["grantId"] = grant_id
                doc["granted"] = str(datetime.utcnow())

            return {
                "autoId": autoId
            }
        else:
            raise RequestTimeout("The user did not approve the auto-change request in time.")
    else:
        raise BadRequest("Plug-in token is not associated with any mobile client.")


def plugin_get_password(_params=None, **kwargs):
    """Partner to :func:`plugin_get_credentials`; if there is more than one user
    for a given website, this function returns the password of just one of the users
    """
    did, key = _params["did"], _params["key"]
    grant_id = None

    # Send the user a push notification to request release of the password.
    plugin_token, url = _params["token"], _params["url"]
    hint_key = f"{url}://{key}"
    tokens = _get_push_tokens(did, plugin_token)
    if len(tokens) > 0:
        body = {
            "url": url,
            "fields": ["username", "password"],
            "plugin": plugin_token
        }

        # Send a push to the user's phone to ask for permission to connect.
        msg = f"Login to {url}?"
        grant_id = _send_push(tokens, "Approve Login", msg, body, did=did)

        if grant_id is not None:
            return _common_get_password(did, hint_key, url)
        else:
            raise RequestTimeout("The user did not approve the login request in time.")
    else:
        raise BadRequest("Plug-in token is not associated with any mobile client.")
    # else:
    #     raise BadRequest("Either `token` or `plugin_token` must be specified.")


def mobile_get_password(_params=None, _headers=None, **kwargs):
    """Partner to :func:`mobile_get_credentials`; if there is more than one user
    for a given website, this function returns the password of just one of the users.
    Assumes that `didauth` has passed already.
    """
    did, key, url = _params["did"], _params["key"], _params["url"]
    hint_key = f"{url}://{key}"
    return _common_get_password(did, hint_key, url)


def _common_fill_form(kind, did, hint_keys, url):
    """Returns the decrypted values for the specified `hint_keys` to respond to
    a plug-in or mobile form fill request.
    """
    results = []
    for hint_key in hint_keys:
        results.append(get_generic_decrypted(kind, did, hint_key))

    return {
        "matches": results,
        "url": url
    }

def plugin_fill_form(_params=None, _data=None, _request=None, **kwargs):
    """Sends a push request to the user to ask for permission to fill out a form
    on a webpage.

    Args:
        _data (dict): that follows the `WebsiteFormRequest` model.
    """
    did = _data["did"]
    hint_keys = _data["fieldSets"]
    token = _data["token"]
    url = _get_url(_data["url"])
    kind = _data["category"]

    # Validate that this is a valid token from the browser.
    tokens = _get_push_tokens(did, token)
    if len(tokens) > 0:
        # Construct a body that tells the user exactly what is happening with their
        # data and passwords.
        fields = set()
        for hint_key in hint_keys:
            fields = fields | get_affected_fields(kind, did, hint_key)

        body = {
            "url": url,
            "fields": tuple(fields),
            "plugin": token
        }

        # Send a push to the user's phone to ask for permission to connect.
        msg = f"Would you like to release this sensitive data to {url}?"
        grant_id = _send_push(tokens, "Release Data", msg, body, did=did)

        if grant_id is not None:
            return _common_fill_form(kind, did, hint_keys, url)
        else:
            raise Unauthorized("Form data was not authorized by user.")
    else:
        raise BadRequest(f"No valid push notification tokens were found for plug-in token {token}.")


def mobile_fill_form(_data=None, **kwargs):
    """Returns decrypted form data for a request from the mobile app. Assumes
    that `didauth` has already taken place.
    """
    did = _data["did"]
    hint_keys = _data["fieldSets"]
    url = _get_url(_data["url"])
    kind = _data["category"]
    return _common_fill_form(kind, did, hint_keys, url)


def plugin_bulk_import(_data=None, **kwargs):
    """Bulk imports vault data in a single request.
    """
    did = _data["did"]
    imports = _data["imports"]
    token = _data["token"]

    tokens = _get_push_tokens(did, token)
    if len(tokens) > 0:
        ids = []
        registry = {}
        for entry in imports:
            kind = entry["category"]
            url = entry["url"]
            if kind not in registry:
                registry[kind] = []
            registry[kind].append(url)
            fieldsets = entry["fieldSets"]
            catkeys = save_generic(kind, did, url, fieldsets, bulk=True)
            ids.extend(catkeys)

        # Finally, update the key registry for the user's keys.
        bulk_key_update(did, registry)

        return {
            "ids": ids
        }
    else:
        raise BadRequest("The given plug-in token is invalid.")


def plugin_store_form(_params=None, _data=None, _request=None, **kwargs):
    """Stores form information for a website for a plug-in.
    """
    did = _data["did"]
    fieldsets = _data["fieldSets"]
    token = _data["token"]
    url = _get_url(_data["url"])
    kind = _data["category"]
    bulk = _data.get("bulk", False)

    tokens = _get_push_tokens(did, token)
    if len(tokens) > 0:
        catkeys = save_generic(kind, did, url, fieldsets, bulk=bulk)
        return {
            "ids": catkeys
        }
    else:
        raise BadRequest("The given plug-in token is invalid.")


def mobile_store_form(_data=None, **kwargs):
    """Stores form information for a website for a mobile app.
    """
    did = _data["did"]
    fieldsets = _data["fieldSets"]
    url = _get_url(_data["url"])
    kind = _data["category"]

    catkeys = save_generic(kind, did, url, fieldsets)
    return {
        "success": True,
        "ids": catkeys
    }


def plugin_search_vault(_params=None, _request=None, **kwargs):
    """Searches the vault for possible matches to form data for a plug-in.
    """
    token = _params["token"]
    did = _params["did"]
    filter = _params["filter"]
    category = _params["category"]
    url = _get_url(_params["url"])

    tokens = _get_push_tokens(did, token)
    if len(tokens) > 0:
        return get_generic_hints(category, did, url, filter)
    else:
        raise BadRequest("The given plug-in token is invalid.")


def mobile_search_vault(_params=None, **kwargs):
    """Searches the vault for possible matches to form data for a mobile app.
    Assumes that `didauth` was already successful.
    """
    did = _params["did"]
    filter = _params["filter"]
    category = _params["category"]
    url = _get_url(_params["url"])

    return get_generic_hints(category, did, url, filter)


def _get_push_timer_delta(token, purpose, key):
    """Returns the number of seconds it has been since the last approved push
    notification for a plug-in token and purpose.

    Args:
        token (str): token of the browser extension to check for.
        purpose (str): what was the notification for?
        key (str): usually a URL, is the key that purpose was approved for.

    Returns:
        tuple: `(delta, grant_id)` with the number of seconds elapsed since the
        grant and the id of the push notification that approved the grant.
    """
    dbid = f"timer/{token}/{purpose}"
    delta, grant_id = 30*24*3600, None # Default delta of a month.
    doc = dbget("fido", dbid)
    if key in doc:
        granted = doc[key]["granted"]
        grant_id = doc[key]["grant_id"]
        delta = (datetime.utcnow()-datetime.strptime(granted, "%Y-%m-%d %H:%M:%S.%f")).seconds

    log.debug(f"Push timer recency has {delta} seconds under {grant_id} for {dbid}.")
    return delta, grant_id


def _update_push_timer(token, purpose, key, grant_id):
    """Updates the push notification timer for a browser plug-in.

    Args:
        token (str): token of the browser extension to check for.
        purpose (str): what was the notification for?
        key (str): usually a URL, is the key that purpose was approved for.
        grant_id (str): the grant_id that approved the push.
    """
    dbid = f"timer/{token}/{purpose}"
    with dblock("fido", dbid) as doc:
        doc[key] = {
            "granted": str(datetime.utcnow()),
            "grant_id": grant_id
        }
        log.debug(f"Stored grant in the push timer for {dbid}.")


def list_user_keys(_params=None, **kwargs):
    """Lists all the keys the user has stored in their vault for a plug-in.
    """
    token = _params["token"]
    did = _params["did"]

    tokens = _get_push_tokens(did, token)
    if len(tokens) > 0:
        # If the user paired the plug-in recently, then just return the keys.
        # Otherwise, send a push asking for permission.
        delta, grant_id = _get_push_timer_delta(token, "list-vault", "browser://extension")

        if delta < 24*60*60 and grant_id is not None: # Approval only required every 24 hours.
            grant_id = f"recent:{grant_id}"
            log.info(f"`list-vault` approved using recency of {delta} seconds and {grant_id}.")
        else:
            log.debug("`list-vault` has not been approved recently; sending push notification.")
            body = {
                "url": "browser://extension",
                "fields": ["usernames", "credit cards (last 4 digits)", "addresses", "secure notes"],
                "plugin": token
            }

            # Send a push to the user's phone to ask for permission to connect.
            msg = f"A browser plug-in is trying to list your password vault contents."
            grant_id = _send_push(tokens, "Populate Browser Plug-in?", msg, body, did=did)
            if grant_id is not None:
                _update_push_timer(token, "list-vault", "browser://extension", grant_id)

        if grant_id is not None:
            return list_vault_keys(did)
        else:
            raise Unauthorized("User did not approve listing of vault keys.")
    else:
        raise BadRequest("The given plug-in token is invalid.")


def mobile_list_user_keys(_params=None, **kwargs):
    """Lists all the keys the user has stored in their vault for a mobile app.
    Assumes that `didauth` has already taken place.
    """
    did = _params["did"]
    return list_vault_keys(did)


def plugin_pair(_params=None, **kwargs):
    """Returns a QR code with a challenge value that can be used to pair a
    browser plug-in with a ClearID mobile app.
    """
    verkey = _params["verkey"]
    qr = qrcode.QRCode(
        error_correction=qrcode.constants.ERROR_CORRECT_M,
        box_size=15,
        border=4,
    )
    challenge = verkey
    qr.add_data(challenge)
    qr.make(fit=True)

    # Store the challenge value so that we can maintain state across requests.
    with dbupdate("fido", f"pair/{challenge}") as doc:
        # Once the user scans the code and grants permission, a token will be
        # generated and stored here. For now, we just create the entry in the
        # database. This is also where store the correlation of user DID to
        # token and challenge.
        doc["token"] = None
        doc["did"] = None
        doc["created"] = str(datetime.utcnow())

    img = qr.make_image(fill_color="black", back_color="white")
    output = BytesIO()
    try:
        img.save(output, format="png")
        result = output.getvalue()
    finally:
        output.close()
    log.debug(f"Database `pair/` entry created for pairing {challenge}.")

    try:
        manager = get_manager()
        event = manager.Event()
        manager.create_event("qr", challenge, event)
    except:
        log.exception(f"Creating QR event for pairing {challenge}.")

    return {
        "challenge": challenge,
        "qrcode": bytes_to_b64(result)
    }


def _get_qevent(challenge):
    """Gets the QR code threading event associated with `challenge`.
    """
    manager = get_manager()
    q_event = manager.get_event("qr", challenge)
    if q_event is None:
        # Something screwed up in the multiprocessing system; check if we
        # have an orphaned database entry to remove.
        try:
            dbdel("fido", f"pair/{challenge}")
        except:
            pass
        raise BadRequest(f"The given challenge parameter `{challenge}` is not valid."
                         "perhaps the pair request timed out.")

    return q_event


def plugin_get_did(_params=None, **kwargs):
    """Gets the user's DID and a plug-in token that authorizes the plug-in to
    send push notifications to the user for logging in.
    """
    manager = get_manager()
    challenge = _params["challenge"]
    q_event = _get_qevent(challenge)

    try:
        # Block the thread to give the user time to scan the code and submit
        # the `/qr` endpoint request.
        check_rpc_obj(q_event, challenge)
        q_event._callmethod("wait", (pair_timeout,))
        if not q_event._callmethod("is_set"):
            raise TimeoutError("Pairing not confirmed within pair timeout limit.")
        with dbupdate("fido", f"pair/{challenge}") as doc:
            token, did = doc["token"], doc["did"]
            vault_seed = doc.get("vault_seed")
            password = doc.get("password")
            doc["paired"] = str(datetime.utcnow())
    except TimeoutError:
        dbdel("fido", f"pair/{challenge}")
        raise RequestTimeout("The QR code didn't get scanned within the pair "
                             "time limit. Refresh the page to start again.")
    finally:
        manager.del_event("qr", challenge)

    return {
        "did": did,
        "token": token,
        "vault_seed": vault_seed,
        "password": password
    }


def plugin_qr_done(_data=None, _request=None, **kwargs):
    """Acknowledges that a user has given permission to pair their device with
    a browser plug-in. It is protected using DID-auth, so if this fires, we
    have already validated the user.
    """
    challenge = _data["challenge"]
    q_event = _get_qevent(challenge)
    push_token = _data["push_token"]
    did = _data["did"]
    vault_seed = _data["vault_seed"]
    password = _data["password"]

    #did = _request.headers["keyId"]

    if q_event is None:
        raise BadRequest("The challenge token is no longer valid; probably "
                         "it has been too long since the QR code was generated. "
                         "Refresh the page to start again.")

    # Generate a new token that the browser plug-in will store as proof of
    # pairing with the user.
    token = gen_salt(48)

    with dbupdate("fido", f"pair/{challenge}") as doc:
        doc.update({
            "token": token,
            "push_token": push_token,
            "did": did,
            "granted": str(datetime.utcnow()),
            "vault_seed": vault_seed,
            "password": password
        })

    # This pairing is equivalent to granting access to the vault list for
    # populating a browser extension.
    _update_push_timer(token, "list-vault", "browser://extension", challenge)

    # Set the multithreading event so that the `get-did` endpoint can return
    # these values.
    if q_event._getvalue() is not None:
        check_rpc_obj(q_event, challenge)
        q_event._callmethod("set")
        return {
            "success": True
        }
    else:
        raise BadRequest("The challenge token is no longer valid; probably "
                         "it has been too long since the QR code was generated. "
                         "Refresh the page to start again.")


def plugin_remove_data(_params=None, **kwargs):
    """Removes sensitive data from the user vault for a mobile app. Assumes that
    `didauth` has already succeeded.
    """
    kind, did, url = _params["category"], _params["did"], _params["url"]
    _id, token = _params["key"], _params["token"]

    tokens = _get_push_tokens(did, token)
    if len(tokens) > 0:
        body = {
            "url": url,
            "fields": [_id],
            "plugin": token
        }

        # Send a push to the user's phone to ask for permission to connect.
        msg = f"Would you like to delete data from your vault?"
        grant_id = _send_push(tokens, "Delete Vault Data?", msg, body, did=did)

        if grant_id is not None:
            delete_generic(kind, did, url, _id)
            return {
                "success": True
            }
        else:
            raise RequestTimeout("User did not approve the deletion in time.")
    else:
        raise BadRequest("No valid plug-in token; cannot perform delete.")


def mobile_remove_data(_params=None, **kwargs):
    """Removes sensitive data from the user vault for a mobile app. Assumes that
    `didauth` has already succeeded.
    """
    kind, did, url = _params["category"], _params["did"], _params["url"]
    _id = _params["key"]
    delete_generic(kind, did, url, _id)
    return {
        "success": True
    }

def mobile_list_pairings(_params=None, **kwargs):
    """Lists all the browser extensions and their corresponding tokens that have been
    paired with a DID.
    """
    did = _params["did"]
    view = get_view("fido", "pairing_did_view")
    rows = view(key=did)["rows"]
    return {
        "did": did,
        "pairings": [r["value"]["plugin_token"] for r in rows
                     if "plugin_token" in r["value"]]
    }

def _revoke_pairing(did, token):
    """Sets the `revoked` key on the pairing entry for `did` and `token`.

    Args:
        did (str): DID of the user that granted access to `token`.
        token (str): browser extension access token to revoke access for.
    """
    view = get_view("fido", "pairing_did_view")
    rows = view(key=did)["rows"]
    if len(rows) == 0:
        raise BadRequest(f"The given plug-in token is not valid for {did}.")

    target = [r["id"] for r in rows if "plugin_token" in r["value"] and r["value"]["plugin_token"] == token]
    if len(target) == 0:
        raise BadRequest(f"The given plug-in token is not valid for {did}.")

    with dbupdate("fido", target[0]) as doc:
        assert "token" not in doc or doc["token"] == token
        doc["revoked"] = str(datetime.utcnow())
        
    return {
        "success": True,
        "message": f"Revoked token {token}"
    }   

def plugin_remove_pairing(_params=None, **kwargs):
    """Unregisters a plug-in from a DID. This can only be requested by the plug-in itself,
    a plug-in can't unregister other plug-ins; only the mobile app can do that.
    """
    did, token = _params["did"], _params["token"]
    return _revoke_pairing(did, token)

def mobile_remove_pairing(_params=None, **kwargs):
    """Unregesters a browser extension pairing, initiated by the mobile app.
    """
    did, token = _params["did"], _params["token"]
    return _revoke_pairing(did, token)