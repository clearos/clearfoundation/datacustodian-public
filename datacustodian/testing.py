"""Helper functions to help access hard-to-reach parts of the code for full
coverage.
"""
import re
from aiohttp import StreamReader
from asyncio import iscoroutinefunction

records = {}
"""dict: keys are int `recid`; values are the `dict` of model properties. This
is just an in-memory cache for testing purposes.
"""
rxvar = re.compile(r"<(?P<kind>\w+):(?P<varname>\w[\w_\d]*)>")

def url_params(url):
    """Discovers the parameters encoded in the URL route.

    Args:
        url (str): URL to discover variables in.
    """
    result = {}
    for match in rxvar.finditer(url):
        rstr = "<{kind}:{varname}>".format(**match.groupdict())
        varname = match.group("varname")
        result[varname] = match.group("kind")

    return result

def urlsub(url, subdict):
    """Substitutes path variables in the specified `url` with their values
    given in the substitution dictionary.

    Args:
        url (str): URL to substitute values in.
        subdict (dict): key-value pairs to substitute in for path variables.
    """
    result = url
    for match in rxvar.finditer(url):
        rstr = "<{kind}:{varname}>".format(**match.groupdict())
        varname = match.group("varname")
        result = result.replace(rstr, str(subdict[varname]))

    return result

#For some reason, the assertions generated by this method pass on the pytest
#side, but the coverage tool says this line isn't getting hit. Skip it...
def exception(**kwargs): # pragma: no cover
    """Creates an unhandled exception.

    Raises:
        ZeroDivisionError: all the time.
    """
    return 1/0.

def save(recid, _data=None, **kwargs):
    """Saves a record from the REST API to file.
    """
    global records
    records[recid] = _data

def query(recid, withname=False, **kwargs):
    """Queries the stored records.

    Args:
        recid (str): record identifier to get.
        withname (bool): argument just to make sure things are wired; doesn't
            do anything.
    """
    rec = records.get(recid, {})
    return {
      "page": 1,
      "pages": 1,
      "per_page": 10,
      "total": 1,
      "items": [rec]
     }


async def handle_client_response(response):
    """Handles a client response from the auto-generated HTTP client for a
    package.

    Args:
        response: tuple containing the result of the request and the status code;
            result is either a :class:`aiohttp.StreamReader`,
            `None`, or a coroutine object for :meth:`aiohttp.ClientResponse.json`,
            or :meth:`aiohttp.ClientResponse.text`.
    """
    result = None
    r, code = response
    if isinstance(r, StreamReader):
        result = await r.read()
    elif iscoroutinefunction(r):
        result = await r()
    else:
        result = r

    return result, code

def _replace_values(v):
    """Handles special indicators on values for test generation.
    """
    handlers = {
        "file://": lambda f: open(f, 'rb')
    }
    result = v
    for k, call in handlers.items():
        if k in v:
            result = call(v)
    return result

def conform_args(args, kwargs):
    """Alters the arguments and keyword arguments to take special indicators
    into account (such as `file://`).

    Returns:
        tuple: (args, kwargs) with updated values where appropriate.
    """
    rargs = []
    for arg in args:
        rargs.append(_replace_values(arg))

    rkwargs = {}
    for k, v in kwargs.items():
        rkwargs[k] = _replace_values(v)

    return rargs, rkwargs

def filter_model_values(model, result):
    """Filters the model values to compare for output to a unit test to separate
    ones to be stored from ones to be compared.

    Args:
        model (dict): expected output from the test routine.
        result (dict): actual output from the routine.

    Returns:
        list: of keys in the model `dict` that should be added to stored state.

    Raises:
        AssertionError: if any of the values for comparison don't match.
    """
    store = []
    for k, v in model.items():
        if v == "<store>":
            store.append(k)
        else:
            assert k in result
            if v == "<not none>":
                assert result[k] is not None
            else:
                assert result[k] == v

    return store
