Shared Application Settings
===========================

To enable auto-generation to work correctly, the specifications for
the generated packages have to be globally available to both
`datacustodian` as well as the generated package
namespace. :mod:`~datacustodian.settings` is a simple module that
provides objects for globally caching specification files.

.. automodule:: datacustodian.settings
   :members:
