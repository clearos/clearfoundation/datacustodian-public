name: OAuth2Client
properties:
  did:
    type: String
    description: >-
      Decentralized identifier for the client based on their master public key.
  redirect_uri:
    type: String
    description: >-
      Default URI to redirect users to after authorization requests are made. Multiple
      URIs can be specified using `\n` between them.
  token_endpoint_auth_method:
    type: String
    description: >-
      Method to use when authenticating the client. Currently, only `client_secret_basic`
      is supported in data custodian.
    default: client_secret_basic
  grant_type:
    type: String
    description: >-
      OAuth2 grant type for which the client is being registered. Supported grant
      types currently are `implicit`, `client-credentials`, `authorization-code`,
      `refresh-token`, `openid-hybrid`, `openid-implicit`.
    default: ""
  response_type:
    type: String
    description: >-
      Type of response that the client seeks. This depends on the grant_type. For
      example, implicit has a response_type of `token`, `code` is another common
      option. Check out the docs for OAuth.
    default: ""
  scope:
    type: String
    description: >-
      Space-separated list of scopes being requested for this client. OpenID
      inclueds a scope called `openid`. Look at the `/scopes` endpoint to get
      a list of supported scopes in the application.
    default: ""
  client_name:
    type: String
    description: Name to associate with the client.
  client_uri:
    type: String
    description: URI that identifiers the client requesting access.
  logo_uri:
    type: String
    description: URI that points to a logo for the client.
  contact:
    type: String
    description: Email or phone number to contact the client at with questions.
  tos_uri:
    type: String
    description: >-
      URI pointing to the terms of service for the client when the user participates
      in OAuth with the client.
  policy_uri:
    type: String
    description: >-
      URI pointing to a privacy policy document; how will the client use the data
      it obtains through OAuth from the user.
  jwks_uri:
    type: String
    description: >-
      URI pointing to the JSON Web Key Set, the set of public keys that can be
      used to verify a JWT issued by the authorization server.
  jwks_text:
    type: String
    description: >-
      An alternative to using a URI for the JWKS, here the keys are included
      as a JSON dictionary following the same format as would be served by the URI.
  i18n_metadata:
    type: String
    description: >-
      Client metadata for dynamic client registration according to RFC7591. See
      https://tools.ietf.org/html/rfc7591#section-2. A JSON dictionary with
      the metadata as key-value pairs.
  software_id:
    type: String
    description: Client-specified software identifier that will be making requests.
  software_version:
    type: String
    description: Version number associated with `software_id`.
