- name: oauth
  description: Implement OAuth and OpenID endpoints for data custodian.
  endpoints:
    - name: scopes
      routes: ['/scopes']
      get:
        docstring: >
          List all the scopes supported by this application, along with descriptions
          of what they grant access to.
        function:
          fqn: datacustodian.identity.oauth.endpoints.scopes_ls
        marshal:
          object: OAuth2ScopeCollection
    - name: register
      description: >
        Register a user in the OAuth system. This endpoint requires DID-based
        authentication (to validate that the user registering is who they say
        they are). This request should include the DID signature of the *user*.
      routes: ['/register']
      post:
        didauth: True
        docstring: >
          Register a new user in the OAuth system.
        function:
          fqn: datacustodian.identity.oauth.endpoints.register
        expect:
          object: OpenIDUser
    - name: client
      description: >
        Create or get clients for interacting with the OAuth system.
      routes: ['/client']
      post:
        didauth: True
        docstring: >
          Create a new client that can be authorized by users to access elements
          of their profile/other functionality. This request should use the DID
          signature of the *user*.
        function:
          fqn: datacustodian.identity.oauth.endpoints.client_create
        expect:
          object: OAuth2Client
        params:
          user_did:
            description: The DID of the user to add this client to.
            required: True
      get:
        docstring: >
          List all the clients that the user has added. This request should
          include the DID signature of the *user*.
        didauth: True
        function:
          fqn: datacustodian.identity.oauth.endpoints.client_ls
        params:
          user_did:
            description: The DID of the user to list clients for.
            required: True
        marshal:
          object: OAuth2ClientList
    - name: authorize
      docstring: >
        Authorize a client to access user resources.
      routes: ['/authorize']
      post:
        docstring: >
          Post an authorization request (from the user) to allow a client to
          access user resources. This is done in response to a check from the
          user via the GET endpoint. This request should include the DID
          signature of the *user*.
        didauth: True
        function:
          fqn: datacustodian.identity.oauth.endpoints.validate
      get:
        docstring: >
          Get the details of the grant request so that the user can confirm it.
          This request should include the DID signature of the *user*.
        function:
          fqn: datacustodian.identity.oauth.endpoints.authorize
        params:
          did:
            description: The DID of the user to request auth from.
            required: True
    - name: token
      docstring: >
        Get an OAuth token for access to user resources.
      routes: ['/token']
      post:
        function:
          fqn: datacustodian.identity.oauth.endpoints.issue_token
    - name: profile
      routes: ['/profile']
      oauth:
        scope: profile
        operator: AND
        optional: False
      get:
        docstring: >
          Return the contents of the user's profile, according to the current
          allowed scope.
        function:
          fqn: datacustodian.identity.oauth.endpoints.profile
        marshal:
          object: OpenIDUser
    - name: revoke
      description: >
        Log out from the OAuth server. This removes any active tokens and
        authorizations that other clients may have. This request should include
        the DID signature of the *user*.
      routes: ['/revoke']
      post:
        didauth: True
        docstring: >
          Trigger logout on the OAuth server.
        function:
          fqn: datacustodian.identity.oauth.endpoints.revoke
        params:
          token:
            description: The token that should be revoked.
            required: True
