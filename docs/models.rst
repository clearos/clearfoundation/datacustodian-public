Data Model Specification YML
============================

As described elsewhere, `models` define data schema that is used to
validate requests are marshal responses. Models loosely follow the
JSON schema format for data `<https://json-schema.org/learn/>`_, only
written with YML syntax.

In its most basic form, the model is described by a collection of
properties, each of which has a type and a description:

.. code-block:: yaml
		
   - name: IFile
     properties:
       did:
         type: String
         description: 'DID of this message being sent/received.'
       url:
         type: String
         description: 'URL to `put` the message to when sending.'
       isdir:
         type: Boolean
         description: 'True if this IFile entry is actually a directory.'

Each model also needs to have a type. Since support for basic types is
provided automatically by the `routes` (see :doc:`namespaces`), for
our purposes the type will always be `object`. If not specified, that
will be the assumed type as well.
	 
When a model should be available globally for all APIs and namespaces,
we recommend using recursive linking (see :doc:`io`).

.. code-block:: yaml
		
   - :../global/model-pagination

More Complicated Scenarios
--------------------------

Following an object-oriented modeling paradigm, new models can also
inherit others. In the following example, a model named `DirectoryLs`
inherits all the properties of the model `Pagination`.

This example also demonstrates how "nesting" can be used to include
references to more complicated structures, for example a list of
models of type `IFile`.

.. code-block:: yaml
		
   - name: DirectoryLs
     inherit: [Pagination]
     properties:
       items:
         type: List
         nest: IFile

For more examples, we recommend viewing a real-world implementation
of `datacustodian` such as `clearshare`, which is available on PyPI.
