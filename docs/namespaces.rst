Namespace/Endpoint Specification YML
====================================

Namespaces are containers for multiple endpoints. Each endpoint
defines a URL to which multiple HTTP methods can be sent. The
`namespaces` attribute in :doc:`application` defines a *list* of
namespaces. A single namespace has the following attributes:

1. **name**: shows up in the generated module file and the swagger
   documentation for the namespace. Is also part of the URL path used
   to query the endpoint.
2. **description**: as for `name`, used for documentation purposes.
3. **endpoints**: a list of endpoint specifications. See the section
   below.

.. code-block:: yaml

   - name: records
     description: "Namespace for all record operations."
     endpoints:
       - endpoint_spec_1
       - endpoint_spec_2
   - name: comms
     description: "Namespace for all secure communications."
     endpoints:
       - endpoint_spec_1
       - endpoint_spec_2
       - ...

Endpoint YML Specification
--------------------------

As described above, `endpoints` attribute for a namespace provides a
*list* of endpoints to configure. Each endpoint has the following
attributes:

1. **name**: shows up in the generated module file and the swagger
   documentation for the endpoint. Is also part of the URL path used
   to query the endpoint.
2. **description**: as for `name`, used for documentation purposes.
3. **routes**: a list of routes that the flask app should configure to
   point to this endpoint. Note that these routes can include
   variables that are parsed automatically from the path and passed as
   keyword arguments to the executing function for the endpoint. See
   the examples below.
4. **<http method>**: any of the common HTTP methods, for example
   `get`, `put`, `post`, `delete`. All lower-case. HTTP methods have
   their own documentation section below.
5. **response**: configure the default HTTP response code and message
   if not unhandled exceptions occur, and no return value is specified
   by the individual HTTP endpoint. It has a `code` and `message`
   attribute as per the example below.

.. code-block:: yaml

   - name: exception
     description: Raises an unhandled exception to test the code.
     routes: ['/exception']
     get: ...
     put: ...
     post: ...
     delete: ...
   - name: 'record'
     response:
       code: 200
       message: "Query returned no records."
     description: 'Query records at a local node.'
     routes: ['/<int:recid>']
     get: ...
     put: ...

HTTP Method Specification
-------------------------

Each endpoint can support any of the HTTP methods as described
above. The attribute for a method spec is just the name of the HTTP
method, like `put`, `post`, `get`, or `delete`. While `patch` is also
supported, we don't have any examples of that in the documentation. An
HTTP method specifies a function to execute when the endpoint is
hit. The function is imported dynamically at runtime using
:func:`datacustodian.utility.import_fqn`.

Supported attributes include:

1. **expect**: use the `object` keyword in this `dict` to specify the
   name of a configured `model` or `parser` that should validate the
   request. This is a reference to
   :meth:`flask_restplus.Namespace.expect`. Note
   that configuring this here also means that the swagger docs show
   the model type details in the documentation. Additional keyword
   arguments expected by that function can be supplied as key-value
   pairs in this dictionary as well.
2. **marshal**: if the output from the function should match a
   configured model, specify the name of that model here. The
   framework will automatically cast the `dict` returned by the
   executing function to the specified model type if possible. Note
   that configuring this here also means that the swagger docs show
   the model type details in the documentation. Note that this a
   wrapper around :func:`flask_restplus.marshal`. Additional keyword
   arguments expected by that function can be supplied as key-value
   pairs in this dictionary as well.
3. **function**: specify which function to execute when this endpoint
   is hit. Because calling functions can be a little complicated, the
   specification for functions has its own section below.
4. **response**: configure the HTTP response code and
   message. Available sub-attributes are `code` and `message`.

.. code-block:: yaml

   put:
     expect:
       object: Record
     function: ...
     response:
       code: 204
       message: "Record updated successfully."
   get:
     marshal:
       object: Collection
       envelope: 'Records'
       skip_none: True
       ordered: True
     function: ...

Function YML Specification
--------------------------

At its simplest, a function only needs a `fqn` to have it handle the
endpoint. For `get` requests, this is typically sufficient. However,
for some more complicated scenarios, additional options are
needed. Before looking at those options, however, it is worth noting:

.. note:: Every function call will have a keyword argument called
   `_data` which is just the deserialized `json` of the request.

.. note:: Every function call will have a keyword argument called
   `_request` which is just the :data:`flask.request` object. Having
   it available means that generated packages don't need to import
   `request` from flask, they can just focus on implementing the
   endpoint logic.

.. warning:: Because of these additional keyword arguments (and for
   forward-compatibility with future versions), we recommend that you
   always include `**kwargs` for *all* your endpoint handlers.

Supported attributes of the `function` keyword are:

1. **fqn**: specify the `.` separated fully-qualified name of the
   function.
2. **kwargs**: if your function requires additional keyword arguments
   (that aren't specified by the request), you can specify these
   here. They are inserted as code in the auto-generated module.

.. code-block:: yaml

   function:
     fqn: datacustodian.testing.query
     kwargs:
       withname: True

Complete Example
----------------

A complete example of the namespace specification file is available at :download:`namespaces.yml <configs/record-namespaces.yml>`.
