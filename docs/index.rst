.. genid documentation master file, created by
   sphinx-quickstart on Fri Jun  7 11:10:03 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

`datacustodian` Privatized Data
===============================

`datacustodian` is a package generating code. It uses YML
specification files to generate new packages that provide custom REST
endpoints to facilitate true privatization of personal data and
identity using distributed ledger and storage technologies. While
`datacustodian` can easily generate many different packages for unique
industries or applications, a lot of the code needed to support this
functionality is common between the
packages. :mod:`~datacustodian.datacustodian_app` houses all the
common functions needed to support generated apps. This means that
many of the generated apps will share specification files around some
of the functionality, while extending it in important,
application-specific ways.

Specification File Documentation
--------------------------------

The heart of `datacustodian` is actually the specification files that
dictate how auto-generated packages will work. The highest-level
specification is the :doc:`application`. It includes references to
specifications for the other specifications, either inline, or through
the :doc:`io`.

.. toctree::
   :maxdepth: 1

   application.rst
   models.rst
   parsers.rst
   namespaces.rst

It is usually easiest to learn from examples. We recommend looking at
the example specification files in the links above. For a real-life
application that uses `datacustodian`, see `clearshare`, which is
available on the PyPI.

API Documentation
-----------------

It is probably easiest to understand the inner workings by starting
with :doc:`app`, and then following the rabbit hole from there.

.. toctree::
   :maxdepth: 1
	     
   app.rst
   api.rst
   io.rst
   ipfs.rst
   msg.rst
   settings.rst
   testing.rst
   utility.rst
   writers.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
