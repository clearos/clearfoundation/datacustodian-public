Terminal Messaging
==================

:mod:`datacustodian.msg` provides wrappers around terminal coloring
and printing functionality. It provides an easy way to provide CLI
feedback to the user with differing levels of verbosity.

.. automodule:: datacustodian.msg
   :members:
