Application Server
==================

As discussed in the :doc:`index` introduction,
:mod:`datacustodian.datacustodian_app` houses common application code
needed to support auto-generated packages that `datacustodian` creates.
     
.. automodule:: datacustodian.datacustodian_app
   :members:
