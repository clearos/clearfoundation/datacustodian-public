Package-generating File Writers
===============================

The general approach to package generation in `datacustodian` is to
provide `jinja` templates in the `templates` directory of the code
repository. Data required to fill those templates is constructed
mainly by the :doc:`app`.

Templates are included as package data with `datacustodian` so that
new packages can be generated easily using the distribution available
via `pip install datacustodian`.

.. automodule:: datacustodian.writers
   :members:
