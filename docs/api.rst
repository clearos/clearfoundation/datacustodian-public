API Server
==========

Because `datacustodian` supports multiple APIs running on multiple
ports, we need to abstract the server into a separate thread that can
run for each of the api-level specificaton files. Specifications for
`parsers` and  `models` are shared across the entire API, so it makes
sense to cache them at the API level as well. These functions are
provided by :mod:`datacustodian.api`.

.. automodule:: datacustodian.api
   :members:
