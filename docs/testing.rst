Unit Testing Support
====================

In normal usage, we don't expect every line of code and logic pathway
to be reached. To exercise common and uncommon pathways in the code,
the :mod:`~datacustodian.testing` provides simple functions that
integrate with the default specification files provided with the docs
for unit testing.

.. automodule:: datacustodian.testing
   :members:
