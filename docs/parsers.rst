Request Parsers Specification YML
=================================

In analogy to a CLI that accepts arguments, `datacustodian` provides
parsers to automatically parse and validate requests. These are merely
a wrapper around those already provided by
:mod:`flask_restplus.reqparse`. For full documentation of request
parsers, we recommend visiting
`<https://flask-restplus.readthedocs.io/en/stable/parsing.html>`_.

Here we include an example of a parser that accepts pagination
arguments.

.. code-block:: yaml

   Pagination:
     - name: page
       type: int
       required: False
       default: 1
       help: 'Page number.'
     - name: per-page
       type: int
       required: False
       choices: [2, 5, 10, 25, 50]
       default: 10
       help: 'Number of records to display in each page.'
		
