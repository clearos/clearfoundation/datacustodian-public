Recursive YML Reader
====================

Despite the generic naming, :mod:`datacustodian.io` mainly supports a
single feature: to recursively read YML files for the
specifications. If any member in a YML file, in any context, starts
with a `:`, it is treated as a relative path to a different YML file
(relative to the path of the root YML file).

The reader recursively visits every member in the specification,
loading additional files if necessary and placing them in the context
that the file link was found. See the `docs/configs` for examples.

.. automodule:: datacustodian.io
   :members:
