#!/bin/sh

set -e

cmd="$@"

until ./wait-for-startup.sh -t 10 -h postgres -p '5432'; do
  >&2 echo "postgres is unavailable - sleeping"
  sleep 1
done

until ./wait-for-startup.sh -t 10 -h couchdb -p '5984'; do
  >&2 echo "couchdb is unavailable - sleeping"
  sleep 1
done

until ./pool-ready.sh; do
  >&2 echo "Ledger pool unavailable - sleeping"
  sleep 1
done
exec $cmd
