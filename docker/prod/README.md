# Production Indy Ledger

This folder has the `docker-compose.yml` files needed to run the `indy-node`
containers for our "production" ledger. At this point in time, the ledger is
still being used for development (because we haven't hardened the security yet).
However, the basic idea is to have an external ledger with different genesis
transactions so that it doesn't have to run in the local development environment.
 
