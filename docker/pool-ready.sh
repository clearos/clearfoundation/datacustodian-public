#!/usr/bin/env bash
function test_pool() {
  export poolconnect=`echo "pool connect indy_pool timeout=3 extended-timeout=3" | indy-cli`
  if [[ $poolconnect == *"\"indy_pool\" has been connected"* ]]; then
    echo 0
  elif [[ $poolconnect == *"\"indy_pool\" has not been connected"* ]]; then
    echo 2
  else
    echo 1
  fi
}

first_test=`test_pool`
if [[ $first_test == 1 ]]; then
  echo "pool create indy_pool gen_txn_file=/home/indy/docs/configs/pool_transactions_genesis" | indy-cli
  exit `test_pool`
else
  exit $first_test
fi
