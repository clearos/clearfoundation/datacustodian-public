FROM bcgovimages/von-image:py36-1.12-0
COPY datacustodian datacustodian
COPY clear-fido fido 
COPY clear-didauth didauth
COPY setup.py setup.cfg README.md requirements.txt /home/indy/
COPY docker/* /home/indy/
COPY docker/deploy/timezone /etc/timezone
USER root

RUN pip install --upgrade pip && \
    pip install -e fido/ && \
    pip install -e didauth/ && \
    pip install --upgrade -r requirements.txt && \
    pip install pytest coverage pytest-cov pytest-asyncio && \
    pip install -e .

RUN git clone https://github.com/rosenbrockc/aries-cloudagent-python.git && \
    cd aries-cloudagent-python && \
    pip install -e . && \
    cd ..

EXPOSE 8020/tcp
EXPOSE 8021/tcp
EXPOSE 8022/tcp
