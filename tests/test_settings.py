import pytest
from attrdict import AttrDict
from datacustodian.settings import load
from datacustodian.utility import relpath

def test_docs():
    """Tests that the configuration examples in the docs get loaded correctly
    using the recursive loader, and that they are :class:`attrdict.AttrDict`.
    """
    target = relpath("docs/configs/records.yml", '.')
    spec = load(target)
    assert isinstance(spec, AttrDict)
    assert isinstance(spec.components[0], AttrDict)
