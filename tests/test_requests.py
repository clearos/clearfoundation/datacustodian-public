import pytest
import requests

def test_save_get(hostname):
    """Tests saving a model via PUT and then retrieving it via GET.
    """
    putdata = {
        "kind": "dog",
        "name": "Talulah"
    }
    recid = 10
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json'
    }

    url = f"http://{hostname}/records/records/{0:d}".format(recid)
    rp = requests.put(url, json=putdata, headers=headers)
    assert rp.status_code == 204

    rg = requests.get(url)
    assert rg.status_code == 200
    assert rg.json()["Records"]["items"][0] == putdata

def test_exception(hostname):
    """Performs a GET to the exception endpoint to raise an unhandled server
    error.
    """
    r = requests.get(f"http://{hostname}/records/records/exception")
    assert r.status_code == 500
