from datacustodian.crypto import otp_gen_key, otp_decrypt, otp_encrypt
from binascii import unhexlify

def b(s):
       return s.encode("latin-1") # utf-8 would cause some side-effects we don't want

def t2b(t):
    if t is None:
        return None
    t2 = t.replace(" ", "").replace("\n", "")
    return unhexlify(b(t2))

def test_otp():
    text = "The quick fox jumped over the lazy dog."
    key = otp_gen_key(text)
    cipher = otp_encrypt(text, key)
    print(cipher)
    otext = otp_decrypt(cipher, key)
    assert otext == text


def test_hkdf():
    """Tests the hierarchical key-derivation function. These tests were taken from `pycryptodome`.
    """
    # Test vectors from RFC5869, Appendix A
    # Each tuple is made up by:
    #       Item #1: secret
    #       Item #2: salt
    #       Item #3: context
    #       Item #4: key_len
    #       Item #5: expected result
    from datacustodian.crypto import hkdf
    _test_vector = (
        (
            "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b",
            "000102030405060708090a0b0c",
            "f0f1f2f3f4f5f6f7f8f9",
            42,
            b'\xdc\xd1\x954\x17\xb1\x19o9\x17)\xc2Lv\xc0\x80K\x96=\x0c\xe5\xee\xa0$6\x13G\xdc\xa8 \xd2\x0f\xd1~`\xc6W\xaf@\x03\xd8r'
        ),
        (
            "000102030405060708090a0b0c0d0e0f" +
            "101112131415161718191a1b1c1d1e1f" +
            "202122232425262728292a2b2c2d2e2f" +
            "303132333435363738393a3b3c3d3e3f" +
            "404142434445464748494a4b4c4d4e4f",
            "606162636465666768696a6b6c6d6e6f" +
            "707172737475767778797a7b7c7d7e7f" +
            "808182838485868788898a8b8c8d8e8f" +
            "909192939495969798999a9b9c9d9e9f" +
            "a0a1a2a3a4a5a6a7a8a9aaabacadaeaf",
            "b0b1b2b3b4b5b6b7b8b9babbbcbdbebf" +
            "c0c1c2c3c4c5c6c7c8c9cacbcccdcecf" +
            "d0d1d2d3d4d5d6d7d8d9dadbdcdddedf" +
            "e0e1e2e3e4e5e6e7e8e9eaebecedeeef" +
            "f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff",
            82,
            b'e\xd7\x04\x9b\xd2\xb2\xa9VpL\xca$\x16\x87Q\xc8\xfd\xfa\x0b\x02\xedf\xbc\xd9\xf9\xbeg9\xe0\xc6\r1\xc9\x86\xbf\xe2\xef\xe91Z*h\xe2\xd9\x81\x922\xf1/Z\xec\xc2\x12\xc7\xc6\xb0a\xed\xe3\xa6\x00Ba\x16\xea\xc2\xf4nD\x0e\x7f4@\xbf\xca\xcd\x81\xaa\x07\xd5\xe5\x8e'
        ))

    for tv in _test_vector:
        secret, salt, context = [ t2b(tv[x]) for x in (0,1,2) ]
        key_len, exp = tv[3], tv[4]

        output = hkdf(secret, salt, context, key_len)
        assert len(output) == key_len
        assert output == exp

def test_master_seed():
    """Tests getting the master seed from environment.
    """
    from datacustodian.crypto import get_master_seed
    assert get_master_seed("default") == b"00000000000000000seed-new-public"

def test_path_to_seed():
    from datacustodian.crypto import keypath_to_salt
    r = keypath_to_salt("/text/don't/work")
    assert r == b'b\x19\xd4P\xa9\xdbY\xf1\x82\xe12\xfe\xff\xf5\xa7\x90nov\xb8\xe3\xd9!\xb2\xab\xb7\xf7\x0c\xe9\xc2\x01A'
