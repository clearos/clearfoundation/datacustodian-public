import pytest
import sys
import requests
from threading import Timer

from datacustodian.datacustodian_app import _parser_options
from datacustodian.utility import relpath
from datacustodian.testing import urlsub

def get_sargs(args):
    """Returns the list of arguments parsed from sys.argv.
    """
    sys.argv = args
    return _parser_options()

def test_args():
    """Makes sure the script examples work properly.
    """
    argv = ["py.test", "docs/configs/records.yml"]
    args = get_sargs(argv)
    assert isinstance(args, dict)
    assert "appspecs" in args
    assert "overwrite" in args

@pytest.mark.asyncio
async def test_examples():
    """Makes sure the script examples work properly.
    """
    from datacustodian import run
    argv = ["py.test", "--examples"]
    args = get_sargs(argv)
    assert args is None
    r = await run()
    assert r is None

def test_urlsub():
    """Tests variable substitution in the URL path.
    """
    url = "/files/<string:reqpath>"
    r = urlsub(url, {"reqpath": "home"})
    assert r == "/files/home"

    url = "/files/<string:reqpath>/<int:recid>"
    r = urlsub(url, {"reqpath": "home", "recid": 10})
    assert r == "/files/home/10"
