# -*- coding: utf-8 -*-
"""Tests the utility functions.
"""
import pytest
from os import path, remove

def test_execution():
    """Tests the execution via shell subprocess in a different folder.
    """
    from datacustodian.utility import execute, reporoot
    sargs = ["pwd"]
    target = path.join(reporoot, "datacustodian")
    xres = execute(sargs, target, nlines=1, env_vars={"ENVVAR":"~/."})
    assert xres["output"][0].strip() == target

    sargs = ["cat dummy-file"]
    target = path.join(reporoot, "datacustodian")
    xres = execute(sargs, target, nlines=1)
    assert len(xres["error"]) > 0

def test_execute():
    """Tests missing lines of execute.
    """
    from datacustodian.utility import execute

    # test early breaking on stderr and stdout.
    temp=execute(('which','python'),'.',nlines=0)
    temp=execute(('which','python'),'.',env_vars={"SOMEVAR":'1'})
    assert 'python' in temp['output'][0]
    assert temp['error'] == []

def test_chdir(tmpdir):
    """Tests the chdir context manager.
    """

    from datacustodian.utility import chdir
    from os import getcwd, mkdir

    target = str(tmpdir.join("chdir"))
    if not path.isdir(target):
        mkdir(target)

    with chdir(target):
        assert getcwd() == target

    cur_dir = getcwd()
    try:
        with chdir(str(tmpdir.join('not_chdir'))):
            l = 1
    except:
        l = 1
    assert getcwd() == cur_dir

def test_rel_path():
    """Tests the relative path.
    """

    from datacustodian.utility import relpath, reporoot

    temp = relpath("./tests")
    assert path.join(reporoot, "tests") == temp

def test_importfqn():
    """Tests dynamic import via FQN.
    """
    from datacustodian.utility import import_fqn
    mod, call = import_fqn("os.path.isfile")
    assert hasattr(call, "__call__")
