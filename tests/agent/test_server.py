import pytest

@pytest.mark.asyncio
async def endpoints(agent):
    r = await agent.server.modules()
    assert r == []

    s = await agent.server.status()
    assert s == {}

    t = await agent.server.reset()
    assert t == {}

    u = await agent.server.protocols('did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/cred*')
    assert any("presentation" in u.keys())
    assert any("issuance" in u.keys())
