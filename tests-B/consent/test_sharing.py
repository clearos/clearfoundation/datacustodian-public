"""Tests the acceptance behavior of agent B when credentials are offered,
issued etc. for ACL.
"""
import pytest


@pytest.mark.asyncio
async def test_purge_exchange(agent):
    """Tests purging all the exchange records accumulated from previous unit
    tests.
    """
    await agent.credentials.exchange_purge()
