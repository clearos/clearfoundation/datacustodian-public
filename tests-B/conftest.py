import pytest
import asyncio
from os import environ

@pytest.fixture(scope='session')
def event_loop():
    """A session-scoped event loop."""
    policy = asyncio.get_event_loop_policy()
    res = policy.new_event_loop()
    asyncio.set_event_loop(res)
    res._close = res.close
    res.close = lambda: None

    yield res

    res._close()

@pytest.fixture(scope="session", autouse=True)
def client(event_loop):
    from datacustodian import app, run, stop, set_loop
    from datacustodian.utility import relpath
    environ["COUCHDB_USERNAME"] = "admin"
    environ["COUCHDB_PASSWORD"] = "password"
    set_loop(event_loop)

    #Before we run the client, make sure that the configured overrides directory
    #doesn't exist.
    from shutil import rmtree
    from os import path
    target = path.expanduser("~/temp/datacustodian")
    if path.isdir(target):
        rmtree(target)

    args = {
        "appspecs": [relpath("docs/configs/coverage.yml", '.'),
                     relpath("docs/configs/records-agent-B.yml", '.')],
        "overwrite": True,
        "debug": True,
        "generate": True,
        "reset_package": True,
        "norun": False,
        "testing": True,
        "nowait": False
    }
    app = event_loop.run_until_complete(run(**args))
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

    stop(None, None)

from datacustodian.base import set_testmode
set_testmode(True)

@pytest.fixture(scope="session", autouse=True)
def hostname(client):
    #`client` argument is just to make sure it is treated as a dependency.
    from datacustodian.settings import app_specs
    return app_specs["server"]["SERVER_NAME"]

@pytest.fixture(scope="session", autouse=True)
def agent(client):
    from datacustodian.dlt import agents
    return agents["default"]

def test_docs(client):
    r = client.get('/ipfs')
    assert r.status_code == 200
